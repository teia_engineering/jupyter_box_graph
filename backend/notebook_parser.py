import json
import uuid
import os
import shutil
import re
from os.path import dirname, abspath

venv_path = dirname(dirname(abspath(__file__))) + "/jbox_venv/"
backend_path = dirname(dirname(abspath(__file__))) + "/backend/"

import subprocess
import sys

import hashlib

# Code from: https://www.programiz.com/
def hash_file(filename):
    h = hashlib.sha1()

    with open(backend_path+"notebooks/"+filename,'rb') as file:
        chunk = 0
        while chunk != b'':
            chunk = file.read(1024)
            h.update(chunk)

    return h.hexdigest()

# Hashing function for ids
def hash_notebook_functions(text):
    h = hashlib.sha1(text.encode())
    return h.hexdigest()

# Installing Packages for Notebook
def install(packages):
    subprocess.check_call([sys.executable, "-m", "pip", "install" ]+packages)

# Getting notebook names from the direcotry of notebooks
def get_notebooks():
    notebooks = []
    directory = os.fsencode(backend_path+"notebooks/")
        
    for f in os.listdir(directory):
        filename = os.fsdecode(f)

        if filename.endswith(".ipynb"): 
           notebooks.append(filename)
    
    return notebooks

# Reading code from notebooks
def json_code(notebook):
    f = open(backend_path+"notebooks/"+notebook, 'r')
    Lines = f.readlines()
    return json.loads("".join(Lines))

# Store data to DB (JSON file for now)
def store_json(data):
    f = open(backend_path+"db.json", 'w')
    Lines = f.writelines(json.dumps(data))
    return 

# Opening file to store new functions
def open_py(f):
    f = open(backend_path+"py_code/"+f.split("/")[-1].split(".")[0]+".py", 'w')
    return f

# Generate function definition when storing.
def generate_def(id):
    return "def " + cells[id]['name'] + "("+cells[id]['inputs'].replace('[','').replace(']','')+"):"

# Getting pydoc from the function.
def get_py_doc(txt):
    regex = r'["""|\'\'\']([\w\s\(\)\.\-\,\;\:]+)["""|\'\'\']'
    x = re.search(regex, txt)
    
    return ' '.join(x.group(1).split()).replace("\n","").replace("\t", "")

notebook_names = get_notebooks()

if len(notebook_names) == 0:
    print("No new files to process")

for book in notebook_names:
    print("Processing notebook: " + book)
    f = open(backend_path+"temp_files/"+book, "w")
    f.write("Now the file has more content!")
    f.close()
    file_id = hash_file(book)
    with open(backend_path+'db.json') as json_file:
        cells = json.load(json_file)
    code = json_code(book)
    py_files = open_py(backend_path+"notebooks/"+file_id+".ipynb")
    
    # f = open(backend_path+"extra_code.py", 'r')
    # lines = f.readlines()
    # if len(lines) > 0:
    #     py_files.writelines(lines)
    for x in code['cells']:
        id = ""
        if x['cell_type'] == "code":
            def_func = False
            pydoc = False
            # Checking for JNB tag on comments.
            if len(x['source']) and  "# jbox" in x['source'][0]:
                splitted = x['source'][0].rstrip().replace("\n", "").replace("# jbox ", "").split(" ")
                if splitted[0] == "installations":
                    print("Installing notebook libraries")

                    # Clean values before going to subprocess.
                    packages = splitted[1:]
                    for ch in ['"',"'", ';','|']:
                        if ch in packages:
                            packages = packages.replace(ch,"")
                    install(packages)
                elif splitted[0] == "imports":
                    print("Adding imports")
                    for t_code in x['source'][1:]:
                        py_files.writelines(t_code)
                    py_files.writelines("\n")
                else:
                    temp = {"name": splitted[0]}
                    arg = ""
                    description = ""
                    for y in splitted[1:]:
                        if "-" == y[0]:
                            arg = y[1:]
                            if arg == "def":
                                def_func = True
                            if arg == "pydoc":
                                pydoc = True
                        else:
                            if arg == "description" and y[-1] != "'":
                                description = description +" "+ y
                            elif arg == "description" and y[-1] == "'":
                                temp[arg] = description + " " + y
                                temp[arg] = temp[arg].strip().replace("'","")
                            else:
                                temp[arg] = y
                    if pydoc:
                        temp['description'] = get_py_doc("".join(x['source']))

                    temp['file_name'] = file_id+".py"
                    temp['type'] = "nb_function"
                    temp['original_file'] = book
                    temp['extra'] = ""
                    id = hash_notebook_functions(str(temp))
                    cells[id] = temp

            if not def_func and id != "":

                py_files.writelines(generate_def(id)+"\n")
            
            for t_code in x['source'][1:]:
                if def_func:
                    py_files.writelines(t_code)

                else:
                    if id != "":
                        py_files.writelines("    "+t_code)

            if not def_func and id != "":
                py_files.writelines("\n    return ["+(cells[id]['outputs'].replace("[","").replace("]",""))+"]")

            py_files.writelines("\n")
            
    store_json(cells)
    shutil.move(backend_path+"notebooks/"+book, backend_path+"notebooks/parsed/"+file_id+".ipynb")
    if os.path.exists(backend_path+"temp_files/"+book):
        print("Temp file removed")
        os.remove(backend_path+"temp_files/"+book)
    else:
        print("The file does not exist")
