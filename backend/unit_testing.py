import unittest
import pathlib
import json
from importlib import import_module
from contextlib import redirect_stdout
import io
import pandas as pd
import glob

def read_db(filename):
    with open(filename, 'r') as f:
        Lines = f.readlines()
        return json.loads("".join(Lines))
    
db = read_db(str(pathlib.Path(__file__).parent.resolve())+"/db.json")

testing_files = []
param_list = ""

for file in glob.glob("test/*.json"):
    testing_files.append(file)

def get_function(name, inputs):
    p, m = name.rsplit('.', 1)
    
    mod = import_module(p)
    met = getattr(mod, m)
    return met(*inputs)

def convert_dataframe(x):
    if 'dataframe' in str(type(x)).lower():
        return json.loads(x.to_json(orient="records"))
    else: 
        return x

def non_tuple(x):
    if 'dataframe' in str(type(x)).lower():
        return convert_dataframe(x)

    elif type(x) == list:
        output = []
        for y in x:
            output.append(convert_dataframe(y))
        return output

    elif type(x) == dict:
        output = []
        for y in x:
            output.append(convert_dataframe(x[y]))
        return output
    else:
        return convert_dataframe(x)

class JBoxEndPointTesting(unittest.TestCase):
    def test_jbox_converted_notebook(self):
        for p in self.param_list:
            db_data = db[p['id']]
            with self.subTest(msg=p['description'] ):
                f = io.StringIO()
                with redirect_stdout(f):
                    output = get_function(".".join(["py_code",db_data["file_name"].split(".py")[0],db_data["name"]]), p['inputs'])
                out_logs = f.getvalue()

                output_list = []

                if "," not in db_data["outputs"] or ("," in db_data["outputs"] and type(output) != tuple):
                    output = (output)

                db_outputs = db_data['outputs'].strip('][')
                if "," in db_outputs:
                    db_outputs = db_outputs.split(",")

                    temp_out = {}
                    for x in range(len(db_outputs)):
                        temp_out[db_outputs[x]] = non_tuple(output[x])
                else:
                    db_outputs = db_outputs.split()

                    temp_out = {}
                    for x in range(len(db_outputs)):
                        temp_out[db_outputs[x]] = non_tuple(output[x])

                self.assertEqual(p['assert_value'], temp_out)

class JBoxTextTestResult(unittest.TextTestResult):
    def addSubTest(self, test, subtest, err):
        super().addSubTest(test, subtest, err)
        self.testsRun+=1
        if err is not None:
            print("Failed", test.filename)
            
if __name__ == '__main__':
    runner = unittest.TextTestRunner(None, resultclass=JBoxTextTestResult, verbosity=2)

    suite = unittest.TestSuite()
    for  x in testing_files:
        with open(x) as f:
            param_list = json.load(f)
        test = JBoxEndPointTesting("test_jbox_converted_notebook")
        test.filename = x
        test.param_list = param_list
        suite.addTest(test)

    runner.run(suite)
