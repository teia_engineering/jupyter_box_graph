from main import app, tasks, db, path
import sys

@app.route(path+'jbox_rest/extra_endpoints', methods=['GET'])
def test():
    return 'Create more end points by adding them in this file.'

@app.route(path+'jbox_rest/get_all_tasks', methods=['GET'])
def get_tasks_info():
    temp = {}
    for x in tasks:
        t = tasks[x]
        t['function'] = ""
        temp[x] = t

    # print(temp,file=sys.stderr)
    return temp