from flask import Flask, request, flash
from flask_cors import CORS
import json
from importlib import import_module
import io
from contextlib import redirect_stdout
import pathlib
from werkzeug.utils import secure_filename
import os
import threading
import uuid
import sys
import extra_configs as ep

NOTEBOOK_UPLOAD_FOLDER = str(pathlib.Path(__file__).parent.resolve())+'/notebooks'
FILE_UPLOAD_FOLDER = str(pathlib.Path(__file__).parent.resolve())+'/files'

ALLOWED_EXTENSIONS_NOTEBOOKS = {'ipynb'}
ALLOWED_EXTENSIONS_INPUTS = {'ipynb', 'csv', 'txt', 'xlsx', 'xls', 'pdf', 'doc', 'png', 'jpeg', "jpg"}

def allowed_file_notebook(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_NOTEBOOKS

def allowed_file_inputs(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_INPUTS

def get_function(name, inputs):
    p, m = name.rsplit('.', 1)
    
    mod = import_module(p)
    met = getattr(mod, m)
    return met(*inputs)

def read_db(filename):
    f = open(filename, 'r')
    Lines = f.readlines()
    return json.loads("".join(Lines))

db = read_db(str(pathlib.Path(__file__).parent.resolve())+"/db.json")

path = "/"
app = None

tasks = {}

# Declaring lock
lock = threading.Lock()


app = Flask(__name__, static_folder='../frontend/build')
app.config['NOTEBOOK_UPLOAD_FOLDER'] = NOTEBOOK_UPLOAD_FOLDER
app.config['FILE_UPLOAD_FOLDER'] = FILE_UPLOAD_FOLDER

for key in ep.extra_file_paths.keys():
    app.config[key] = str(pathlib.Path(__file__).parent.resolve()) + ep.extra_file_paths[key]

for key in ep.extra_configs.keys():
    app.config[key] = ep.extra_configs[key]

CORS(app)

@app.route(path)
def index():
    return app.send_static_file('index.html')

@app.route(path+"jbox_rest", methods=['GET'])
def get_db():
    db = read_db(str(pathlib.Path(__file__).parent.resolve())+"/db.json")
    return db

@app.route(path+"jbox_rest/input_upload_file", methods=['POST'])
def input_upload_file():
    if request.method == 'POST':
        print(request.files)
        # check if the post request has the file part
        if 'file' not in request.files:
            return {"children":[],"result":{"logs":['Missing part as paramerter'],"output":{}},"status":"FAILURE","task_id":"","traceback":"Missing part as paramerter"}
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            return {"children":[],"result":{"logs":['No File Selected'],"output":{}},"status":"FAILURE","task_id":"","traceback":"No File Selected"}
        if file and allowed_file_inputs(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['FILE_UPLOAD_FOLDER'], filename))
            info = "{} saved in server.".format(filename)
            return {"children":[],"result":{"logs":[info],"output":{'dir': os.path.join(app.config['FILE_UPLOAD_FOLDER'], filename)}},"status":"SUCCESS","task_id":"","traceback":info}
            
        else:
            info = "File with non-allowable extension. Allowed externsions are: {}".format(",".join(ALLOWED_EXTENSIONS_INPUTS))
            return {"children":[],"result":{"logs":[info],"output":{}},"status":"FAILURE","task_id":"","traceback":info}

@app.route(path+"jbox_rest/upload_notebook", methods=['POST'])
def upload_notebook():
    if request.method == 'POST':
        print(request.files)
        # check if the post request has the file part
        if 'file' not in request.files:
            return {"children":[],"result":{"logs":['Missing part as paramerter'],"output":{}},"status":"FAILURE","task_id":"","traceback":"Missing part as paramerter"}

        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            return {"children":[],"result":{"logs":['No File Selected'],"output":{}},"status":"FAILURE","task_id":"","traceback":"No File Selected"}

        if file and allowed_file_notebook(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['NOTEBOOK_UPLOAD_FOLDER'], filename))
            info = "{} saved in server.".format(filename)
            return {"children":[],"result":{"logs":[info],"output":{}},"status":"SUCCESS","task_id":"","traceback":info}
        else:
            info = "File with non-allowable extension. Allowed externsions are: {}".format(",".join(ALLOWED_EXTENSIONS_NOTEBOOKS))
            return {"children":[],"result":{"logs":[info],"output":{}},"status":"FAILURE","task_id":"","traceback":info}

@app.route(path+"jbox_rest/functions_by_files", methods=['POST'])
def functions_by_files():
    outputs = {}
    data = json.loads(request.data)

    if "files" in data:
        for f in data['files']:
            for k in db.keys():
                if f == db[k]['original_file']:
                    outputs[k] = db[k]

    returnable_obj = {"children":[],"result":{"logs":[],"output":{}},"status":"SUCCESS","task_id":"","traceback":None}
    returnable_obj['result']['output'] = outputs
    return returnable_obj

def convert_dataframe(x):
    if 'dataframe' in str(type(x)).lower():
        return json.loads(x.to_json(orient="records"))
    else: 
        return x

def non_tuple(x):
    if 'dataframe' in str(type(x)).lower():
        return convert_dataframe(x)

    elif type(x) == list:
        output = []
        for y in x:
            output.append(convert_dataframe(y))
        return output

    elif type(x) == dict:
        output = {}
        for y in x:
            output[y] = convert_dataframe(x[y])
        return output
    else:
        return convert_dataframe(x)

# Thread for Executing Code from Block.
def executing_box_thread(task_id,db_data,inputs):
    global tasks
    f = io.StringIO()
    with redirect_stdout(f):
        output = get_function(".".join(["py_code",db_data["file_name"].split(".py")[0],db_data["name"]]), inputs)
    out_logs = f.getvalue()
    
    if out_logs == "":
        out_logs= []
    if "," not in db_data["outputs"] or ("," in db_data["outputs"] and type(output) != tuple):
        output = (output)

    db_outputs = db_data['outputs'].strip('][')
    if "," in db_outputs:
        db_outputs = db_outputs.split(",")

        temp_out = {}
        for x in range(len(db_outputs)):
            temp_out[db_outputs[x]] = non_tuple(output[x])
    else:
        db_outputs = db_outputs.split()

        temp_out = {}
        for x in range(len(db_outputs)):
            temp_out[db_outputs[x]] = non_tuple(output[x])
    lock.acquire()
    tasks[task_id] = {"children":[],"result":{"logs":out_logs,"output":temp_out},"status":"SUCCESS","task_id":task_id,"traceback":None}
    lock.release()

# Check status of block execution.
@app.route(path+'jbox_rest/task_status', methods=['POST'])
def get_task_status(): 
    data = json.loads(request.data)
    if "job_id" in data and data['job_id'] in tasks.keys():
        return tasks[data['job_id']]
    else:
        info = "Job ID not found on system. Please try another ID"
        return {"children":[],"result":{"logs":[info],"output":{}},"status":"FAILURE","task_id":"","traceback":info}

# Check status of block execution.
@app.route(path+'jbox_rest/check_nb_parsing', methods=['GET'])
def check_nb_parsing(): 
    filenames = next(os.walk("temp_files"), (None, None, []))[2]  # [] if no file
    return {"files":filenames}

# Executing code block rest end point.
@app.route(path+"jbox_rest/execute_box", methods=['POST'])
def post_box_execution():
    global tasks
    db = read_db(str(pathlib.Path(__file__).parent.resolve())+"/db.json")
    task_id = str(uuid.uuid4())
    inputs = []
    box_id = ""
    data = json.loads(request.data)
    
    if "id" in data:
        box_id = data['id']

    else:
        info = "ID is required."
        return {"children":[],"result":{"logs":[info],"output":{}},"status":"FAILURE","task_id":"","traceback":info}

    if "metadata" not in data:
        data['metadata'] = {}

    if "options" not in data:
        data['options'] = {}

    # Table Display return input as output.
    if box_id == "output0":
        info = "Output for table: "+str(data['inputs'])
        return {"children":[],"result":{"logs":[info],"output":{"output":data['inputs']}},"status":"SUCCESS","task_id":"","traceback":info}
    if box_id == "output2":
        info = "Output for map: "+str(data['inputs'])
        return {"children":[],"result":{"logs":[info],"output":{"output":data['inputs']}},"status":"SUCCESS","task_id":"","traceback":info}
    if box_id == "output3":
        info = "Output for graphs: "+str(data['inputs'])
        return {"children":[],"result":{"logs":[info],"output":{"output":data['inputs']}},"status":"SUCCESS","task_id":"","traceback":info}
    if box_id in db:
        db_data = db[box_id]
        db_inputs = db_data['inputs'].strip('][')
        if "," in db_inputs:
            db_inputs = db_inputs.split(",")
        else:
            db_inputs = db_inputs.split()

        if "inputs" in data and len(db_inputs) > 0:
            inputs = data['inputs']
        
        elif "inputs" not in data and len(db_inputs) > 0:
            info = "Inputs required for this box."
            return {"children":[],"result":{"logs":[info],"output":{}},"status":"FAILURE","task_id":"","traceback":info}
        
        else:
            inputs = []

        # print(data,file=sys.stderr)
        # Add fucntions for here
        for f in ef.before_lock:
            data = f(data)
        # Acquiring lock to add task
        lock.acquire()
        tasks[task_id] = {"job_id":task_id,"status":"PENDING","output":[], 'error':False, "logs":"", 'db_data':db_data, "metadata":data['metadata'], 'options':data['options']}
        lock.release()
        
        thread = threading.Thread(target=executing_box_thread, args=(task_id,db_data,inputs,))
        thread.daemon = True  
        thread.start()
        
        return {"children":[],"result":{"logs":[],"output":{"job_id":task_id}},"status":"SUCCESS","task_id":"","traceback":None}
    else:
        info = "ID provided does not match any in the DB.json. Please try a new value."
        return {"children":[],"result":{"logs":[info],"output":{}},"status":"FAILURE","task_id":"","traceback":info}
        
import extra_endpoints
import extra_functions as ef
