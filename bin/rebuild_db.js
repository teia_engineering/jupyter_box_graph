#! /usr/bin/env node

var dir_location = __dirname.substring(0, __dirname.length-3);
const myArgs = process.argv.slice(2);

var shell = require("shelljs");

let rerun_notebooks = false;
let celery = false

for (let step = 0; step < myArgs.length; step++) {
    if(myArgs[step] == "--rerun_notebooks"){
        rerun_notebooks = true;
    }
    else if(myArgs[step] == "--celery"){
        celery = true;
    }
    step += 1;
}

if(rerun_notebooks && !celery){
    shell.exec('echo "Moving Notebooks to rerun folder. Cron Job will run on the shedule."');
    shell.exec('mv '+dir_location+'backend/notebooks/parsed/* '+dir_location+'backend/notebooks/ ');
}

else if(rerun_notebooks && celery){
    shell.exec('echo "Moving Notebooks to rerun folder. Cron Job will run on the shedule."');
    shell.exec('mv '+dir_location+'rabbitmq/backend/notebooks/parsed/* '+dir_location+'rabbitmq/backend/notebooks/ ');
}

if(!celery){
    // Starting Server backend
    shell.exec('echo "Resetting DB"');
    shell.exec('echo "{}" > '+dir_location+'backend/db.json');
    shell.exec('echo "DB file can be found at '+dir_location+'backend/db.json"');
    shell.exec('echo "Finished resetting DB."');
}

else{
    // Starting Server backend
    shell.exec('echo "Resetting DB"');
    shell.exec('echo "{}" > '+dir_location+'rabbitmq/backend/db.json');
    shell.exec('echo "DB file can be found at '+dir_location+'rabbitmq/backend/db.json"');
    shell.exec('echo "Finished resetting DB."');
}
