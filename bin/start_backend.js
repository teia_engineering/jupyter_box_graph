#! /usr/bin/env node

var dir_location = __dirname.substring(0, __dirname.length-3);
const myArgs = process.argv.slice(2);
let port = 9494

var shell = require("shelljs");
const fs = require('fs');
var chokidar = require('chokidar');
let url_path = "/"
let log_cron = false;

for (let step = 0; step < myArgs.length; step++) {
    if(myArgs[step] == "--backend_port"){
        port = myArgs[step+1]
        console.log("Setting port")
    }
    else if(myArgs[step] == "--url_path"){
        url_path = myArgs[step+1]
        console.log("Setting url path "+url_path)
    }
    step += 1;
}

shell.exec('echo "Starting backend!"')
try {
    const data = fs.readFileSync(dir_location+'backend/jbox_unicorn.pid', 'utf8');
    let processId = data.split("\n")[0]
    shell.exec('echo "Killing process "'+processId)
    shell.exec("kill "+processId);
    shell.exec('echo "Process has been stopped..."')
} catch (err) {
    shell.exec('echo "'+err+'"')
    shell.exec('echo "No instance running, lets start a new one..."')

}
var watcher = chokidar.watch(dir_location+"backend/notebooks", {ignored: /^\./, persistent: true, awaitWriteFinish: true, ignoreInitial: true});
shell.exec('echo "Watching files in notebooks directory for updates..."')
watcher
    .on('add', function(path) { 
        splitted = path.split(".")
        console.log(path)
        if(path.length > 1 && splitted[1] == "ipynb"){
            console.log("File change: ", path)
            if(log_cron){
                shell.exec(dir_location+'jbox_venv/bin/python '+dir_location+'backend/notebook_parser.py >> '+dir_location+"backend/logs/cron.log")
            }
            else{
                shell.exec(dir_location+'jbox_venv/bin/python '+dir_location+'backend/notebook_parser.py')
            }
        }            
    })

shell.exec(dir_location+'jbox_venv/bin/gunicorn -b 0.0.0.0:'+port+' --pid '+dir_location+'backend/jbox_unicorn.pid --chdir '+dir_location+'backend/ main:app --daemon')


