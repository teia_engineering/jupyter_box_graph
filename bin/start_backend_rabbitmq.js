#! /usr/bin/env node

var dir_location = __dirname.substring(0, __dirname.length-3);
const myArgs = process.argv.slice(2);
let port = 9494

var shell = require("shelljs");
const fs = require('fs');
var chokidar = require('chokidar');

let log_cron = false;

let celery_path = "rabbitmq/"
let url_path = "/"

const startStopCelery = () => {
    try {
        let data = fs.readFileSync(dir_location+'rabbitmq/celery_process/worker1.pid', 'utf8');
        let processId = data.split("\n")[0]
        shell.exec('echo "Killing process "'+processId)
        shell.exec("kill "+processId);
        shell.exec('echo "Process has been stopped..."')
    } catch (err) {
        shell.exec('echo "No instance running, lets start a new one..."')
    }

    // Starting Server backend
    shell.cd(dir_location+"/rabbitmq/backend/")
    shell.exec(dir_location+'/jbox_venv/bin/celery -A celery_config.celery_app multi start worker1 \
                --pidfile="'+dir_location+'/rabbitmq/celery_process/%n.pid" \
                --logfile="'+dir_location+'/rabbitmq/celery_log/%n%I.log"')
}


for (let step = 0; step < myArgs.length; step++) {
    if(myArgs[step] == "--backend_port"){
        port = myArgs[step+1]
        console.log("Setting port")
    }
    else if(myArgs[step] == "--url_path"){
        url_path = myArgs[step+1]
        console.log("Setting url path "+url_path)
    }
    step += 1;
}

const runEverything = () => {
    shell.exec('echo "Checking Celery Worker running already..."')
    
    startStopCelery();

    shell.exec('echo "Starting backend!"')
    try {
        const data = fs.readFileSync(dir_location+'rabbitmq/backend/jbox_unicorn.pid', 'utf8');
        let processId = data.split("\n")[0]
        shell.exec('echo "Killing process "'+processId)
        shell.exec("kill "+processId);
        shell.exec('echo "Process has been stopped..."')
    } catch (err) {
        shell.exec('echo "No instance running, lets start a new one..."')

    }

    shell.exec('echo "Starting flask backend..."')
    shell.exec(dir_location+'jbox_venv/bin/gunicorn -b 0.0.0.0:'+port+' --pid '+dir_location+'/rabbitmq/backend/jbox_unicorn.pid --chdir '+dir_location+'rabbitmq/backend/  main:app --daemon')

    shell.exec('echo "Watching files in notebooks directory for updates..."')
    var watcher = chokidar.watch(dir_location+"/rabbitmq/backend/notebooks", {ignored: /^\./, persistent: true, awaitWriteFinish: true, ignoreInitial: true});
    watcher
        .on('add', function(path) { 
            splitted = path.split(".")
            if(path.length > 1 && splitted[1] == "ipynb"){
                console.log("File change: ", path)
                if(log_cron){
                    shell.exec(dir_location+'jbox_venv/bin/python '+dir_location+celery_path+'backend/notebook_parser.py >> '+dir_location+celery_path+"backend/logs/cron.log")
                }
                else{
                    shell.exec(dir_location+'jbox_venv/bin/python '+dir_location+celery_path+'backend/notebook_parser.py')
                }
                startStopCelery()
            }            
        })
}

// Checking for RabbitMQ
const { stdout, stderr, code } = shell.exec(dir_location+'jbox_venv/bin/python '+dir_location+'rabbitmq/checkRabbitStatus.py ') 

if(stdout.includes("RabbitMQ running on server!")){
    runEverything()    
}
else{
    // No RabbitMQ running
    shell.exec('echo "No RabbitMQ on localhost. This does not take into account any configurations of celery."')

    const readline = require("readline");
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    let answer = "n";

    rl.question("Would you like to continue? (y/n) ", function(cont) {
        answer = cont
        rl.close();
    });
    
    rl.on("close", function() {
        if(answer == "y"){
            runEverything()
        }
        else{
            shell.exec('echo "Exiting initialization"')
            process.exit(0);
        }
    });
}




