#! /usr/bin/env node

const fs = require('fs');
const package_json = '../frontend/package.json';
const package_json_file = require(package_json);
    
package_json_file.homepage = ".";

const dir_location = __dirname.substring(0, __dirname.length-3);
const myArgs = process.argv.slice(2);
let port = 9191
let backend_port = 9494;
let url_path = ""

var shell = require("shelljs");

for (let step = 0; step < myArgs.length; step++) {
    if(myArgs[step] == "--frontend_port"){
        port = myArgs[step+1]
        console.log("Setting port")
    }
    else if(myArgs[step] == "--backend_port"){
        backend_port = myArgs[step+1]
        console.log("Setting backend port: "+backend_port)
    }
    else if(myArgs[step] == "--url_path"){
        url_path = myArgs[step+1]
        console.log("Setting url path: "+url_path)
        package_json_file.homepage = url_path;
    }
    step += 1;
}
    
// fs.writeFile(package_json, JSON.stringify(package_json_file, null, 2), function writeJSON(err) {
//     if (err) return console.log(err);
//     console.log('writing to ' + package_json);
//     // Starting Front End
//     shell.exec('echo "Starting Front End!"')
//     shell.exec('export PORT='+port+'; REACT_APP_BACKEND_PORT='+backend_port+' REACT_APP_URL_PATH='+url_path+' npm start --prefix '+dir_location+'frontend');
// });

shell.exec('echo "Starting Front End!"')
    shell.exec('export PORT='+port+'; REACT_APP_BACKEND_PORT='+backend_port+' REACT_APP_URL_PATH='+url_path+' npm start --prefix '+dir_location+'frontend');
