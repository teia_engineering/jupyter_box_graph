#! /usr/bin/env node

var dir_location = __dirname.substring(0, __dirname.length-3);

var shell = require("shelljs");

const myArgs = process.argv.slice(2);

const fs = require('fs');
const package_json = '../frontend/package.json';
const package_json_file = require(package_json);
package_json_file.homepage = ".";
let url_path = ""
let build_frontend = true;

// Creating Virtual Env
shell.exec('echo "Creating Virtual Env"')
shell.exec('python3 -m venv '+dir_location+'jbox_venv')

// Installing dependencies
shell.exec('echo "Installing python dependencies"')
shell.exec(dir_location+'/jbox_venv/bin/pip install -r '+dir_location+'/backend/requirements.txt')
shell.exec('mkdir '+dir_location+'backend/notebooks')
shell.exec('mkdir '+dir_location+'backend/notebooks/parsed')
shell.exec('mkdir '+dir_location+'backend/files')
shell.exec('mkdir '+dir_location+'backend/py_code')
shell.exec('mkdir '+dir_location+'rabbitmq/backend/notebooks')
shell.exec('mkdir '+dir_location+'rabbitmq/backend/notebooks/parsed')
shell.exec('mkdir '+dir_location+'rabbitmq/backend/files')
shell.exec('mkdir '+dir_location+'rabbitmq/backend/py_code')


// Installing dependencies
shell.exec('echo "Installing node dependencies"')
shell.exec('npm install --prefix '+dir_location+'frontend')

for (let step = 0; step < myArgs.length; step++) {
    if(myArgs[step] == "--url_path"){
        url_path = myArgs[step+1]
        console.log("Setting url path: "+url_path)
        package_json_file.homepage = url_path;
        step += 1;
    }
    else if (myArgs[step] == "--no_frontend"){
        build_frontend = false;
    }
}

if (build_frontend){
    fs.writeFile(package_json, JSON.stringify(package_json_file, null, 2), function writeJSON(err) {
        if (err) return console.log(err);
        console.log('writing to ' + package_json);
        // Starting Front End
        shell.exec('echo "Building Front End!"')
        shell.exec('REACT_APP_URL_PATH='+url_path+' npm run build --prefix '+dir_location+'frontend');
    
        // Completed
        shell.exec('echo "Finished installing dependencies."')
        shell.exec('echo "Environment can be found at '+dir_location+'jbox_venv"')
    
    });
} else {
    shell.exec('echo "Finished installing dependencies."')
    shell.exec('echo "Environment can be found at '+dir_location+'jbox_venv"')
}


