#! /usr/bin/env node

const myArgs = process.argv.slice(2);
let front_end_port = 9191
let back_end_port = 9494
let log_cron = false
var dir_location = __dirname.substring(0, __dirname.length-3);
var celery = false;
let celery_path = ""
let answer = "y";

var shell = require("shelljs");
const fs = require('fs');
var chokidar = require('chokidar');
let url_path = ""

for (let step = 0; step < myArgs.length; step++) {
    if(myArgs[step] == "--frontend_port"){
        front_end_port = myArgs[step+1]
        console.log("Setting frontend port: "+front_end_port)
    }

    else if(myArgs[step] == "--backend_port"){
        back_end_port = myArgs[step+1]
        console.log("Setting backend port: "+back_end_port)
    }

    else if(myArgs[step] == "--cron_log"){
        log_cron = true
        console.log("Using Logs for NB Parser.")
    }
    else if(myArgs[step] == "--celery"){
        celery = true
        console.log("Using Celery for backend...")
    }
    else if(myArgs[step] == "--url_path"){
        url_path = myArgs[step+1]
        console.log("Setting url path "+url_path)
    }
    step += 1;
}

if(celery){
    celery_path = "rabbitmq/"
}

if(log_cron){
    console.log("NB Parser logs can be found in the following location: "+dir_location+celery_path+"backend/logs/cron.log")
}
const startStopCelery = () => {
    try {
        let data = fs.readFileSync(dir_location+'rabbitmq/celery_process/worker1.pid', 'utf8');
        let processId = data.split("\n")[0]
        shell.exec('echo "Killing process "'+processId)
        shell.exec("kill "+processId);
        shell.exec('echo "Process has been stopped..."')
    } catch (err) {
        shell.exec('echo "No instance running, lets start a new one..."')
    }

    // Starting Server backend
    shell.cd(dir_location+"/rabbitmq/backend/")
    shell.exec(dir_location+'/jbox_venv/bin/celery -A celery_config.celery_app multi start worker1 \
                --pidfile="'+dir_location+'/rabbitmq/celery_process/%n.pid" \
                --logfile="'+dir_location+'/rabbitmq/celery_log/%n%I.log"')
}

const startGunicorn = () => {
    if(answer == "y"){
        try {
            const data = fs.readFileSync(dir_location+celery_path+'backend/jbox_unicorn.pid', 'utf8');
            let processId = data.split("\n")[0]
            shell.exec('echo "Killing process "'+processId)
            shell.exec("kill "+processId);
            shell.exec('echo "Process has been stopped..."')
        } catch (err) {
            shell.exec('echo "'+err+'"')
            shell.exec('echo "No instance running, lets start a new one..."')
        
        }
        
        shell.exec(dir_location+'jbox_venv/bin/gunicorn  -b 0.0.0.0:'+back_end_port+' --pid '+dir_location+celery_path+'backend/jbox_unicorn.pid  --chdir '+dir_location+celery_path+'backend/ "main:create_app(\'/'+url_path+'/\')"  --daemon')
        
        shell.exec('echo "JBOX has been started. Port for deployment is '+back_end_port+'"')
        
        var watcher = chokidar.watch(dir_location+celery_path+"backend/notebooks", {ignored: /^\./, persistent: true, awaitWriteFinish: true, ignoreInitial: true});
        shell.exec('echo "Watching files in notebooks directory for updates..."')
        watcher
            .on('add', function(path) { 
                if(path.split(".")[1] == "ipynb"){
                    console.log("File change: ", path)
                    if(log_cron){
                        shell.exec(dir_location+'jbox_venv/bin/python '+dir_location+celery_path+'backend/notebook_parser.py >> '+dir_location+celery_path+"backend/logs/cron.log")
                    }
                    else{
                        shell.exec(dir_location+'jbox_venv/bin/python '+dir_location+celery_path+'backend/notebook_parser.py')
                    }
                    if(celery){
                        startStopCelery()
                    }
                }            
            })
    }
}

shell.exec('echo "Starting backend!"')
if(celery){
    shell.exec('echo "Checking Celery Worker running already..."')

    // Checking for RabbitMQ
    const { stdout, stderr, code } = shell.exec(dir_location+'jbox_venv/bin/python '+dir_location+'rabbitmq/checkRabbitStatus.py ') 

    if(stdout.includes("RabbitMQ running on server!")){
        startStopCelery()
        startGunicorn()  
    }
    else{
        // No RabbitMQ running
        shell.exec('echo "No RabbitMQ on localhost. This does not take into account any configurations of celery."')

        const readline = require("readline");
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.question("Would you like to continue? (y/n) ", function(cont) {
            answer = cont
            rl.close();
        });
        
        rl.on("close", function() {
            if(answer == "y"){
                startStopCelery()
                startGunicorn()
            }
            else{
                shell.exec('echo "Exiting initialization"')
                process.exit(0);
            }
        });
    }
}
else{
    startGunicorn()
}



