#! /usr/bin/env node

var dir_location = __dirname.substring(0, __dirname.length-3);
const GENERIC_COMMANDS = [
  { "Desctiption":"JBox Python Environment", "Path":dir_location+'jbox_venv'},
  { "Desctiption":"JBox Front End", "Path":dir_location+'frontend'},
]

const CELERY_RABBIT_COMMANDS = [
  { "Desctiption":"Uploaded/Created Files Directory", "Path":dir_location+'rabbitmq/backend/files'},
  { "Desctiption":"Notebooks to be Parsed Directory", "Path":dir_location+'rabbitmq/backend/notebooks'},
  { "Desctiption":"Celery Config file to configurate Celery", "Path":dir_location+'rabbitmq/backend/celery_config/celeryconfig.py'},
  { "Desctiption":"Unittesting directory of endpoints", "Path":dir_location+'rabbitmq/backend/test'},
  { "Desctiption":"File to add new endpoints", "Path":dir_location+'rabbitmq/backend/extra_endpoints.py'},
  { "Desctiption":"File to add extra functions to Flask", "Path":dir_location+'rabbitmq/backend/extra_functions.py'},
  { "Desctiption":"File to add extra file paths to access from Flask", "Path":dir_location+'rabbitmq/backend/extra_paths.py'},
  { "Desctiption":"GUnicorn Process Id", "Path":dir_location+'rabbitmq/backend/jbox_unicorn.pid'},
  { "Desctiption":"Celery Process Id", "Path":dir_location+'rabbitmq/celery_process/celery.pid'},
  { "Desctiption":"JBox Database File (db.json)", "Path":dir_location+'rabbitmq/backend/db.json'},
]

const COMMANDS = [
  { "Desctiption":"Uploaded/Created Files Directory", "Path":dir_location+'backend/files'},
  { "Desctiption":"Notebooks to be Parsed Directory", "Path":dir_location+'backend/notebooks'},
  { "Desctiption":"Unittesting directory of endpoints", "Path":dir_location+'backend/test'},
  { "Desctiption":"File to add new endpoints", "Path":dir_location+'backend/extra_endpoints.py'},
  { "Desctiption":"File to add extra functions to Flask", "Path":dir_location+'backend/extra_functions.py'},
  { "Desctiption":"File to add extra file paths to access from Flask", "Path":dir_location+'backend/extra_paths.py'},
  { "Desctiption":"GUnicorn Process Id", "Path":dir_location+'backend/jbox_unicorn.pid'},
  { "Desctiption":"JBox Database File (db.json)", "Path":dir_location+'backend/db.json'},
]

console.log("Below you will find all the directories associated to JBox on the system.")
console.log("")
console.log("Below are generic JBox pahs:")
console.table(
  GENERIC_COMMANDS.map(command => {
      return {
        "Description": command.Desctiption,
        "Path": command.Path
      };
    })
  );
console.log("")
console.log("Below are paths for the RabbitMQ/Celery backend:")
console.table(
  CELERY_RABBIT_COMMANDS.map(command => {
      return {
        "Description": command.Desctiption,
        "Path": command.Path
      };
    })
  );
  console.log("")
  console.log("Below are paths for the non-RabbitMQ/Celery backend:")
  console.table(
      COMMANDS.map(command => {
        return {
          "Description": command.Desctiption,
          "Path": command.Path
        };
      })
    );