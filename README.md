# Jupyter Boxes
[![Documentation Status](https://readthedocs.org/projects/jupyter-boxes/badge/?version=latest)](https://jupyter-boxes.readthedocs.io/en/latest/?badge=latest)

Jupyter Boxes is a framework that allows a user to create a drag and drop workflow utilizing Jupyter notebook cells. The framework is build on top of [React Flow](https://reactflow.dev/). A user will add comments to the beginning of a cell that will tell the framework how to convert it into a usable block. The framwork is composed a back-end/front-end (port `9494`). The backend deployed contains the code for the frontend and the parsing code for the backend. File watcher looks at the notebook directory to put new notebooks to be processed automatically. Once processed, the notebook is not looked at again (unless db reset). The processed information get stored into a file called `db.json`. This file contians all the blocks to display.

<img src='https://gitlab.com/teia_engineering/jupyter_box_graph/-/raw/main/img/example.png' width="700px" height="358">

## Getting Started with Jupyter Boxes

To install Jupyter boxes run the following command

    npm install jupyter_boxes -g

Once installed, the library will contain a set of commands that can be run from the terminal. Those are the following:

### jbox_build
This command will build the python virtualenv for the backend to work. Will install all required libraries into a directory containing the vierual environment called `jbox_venv`.

### jbox_reset_db
This command will reset the backend "database" containing the notebooks that have been parsed. This will take any notebooks in the parsed directory and return them to be parsed again. It also restarts the db.json "database" file.

### jbox_start
This command will start the whole framework. This includes the backend, frontend, and cron schedule. There are two optional parameters, one for setting the backend-port and the second to allow logs to be created for the cron job. This file could be big so take into consideration. To run on the background add the `&` at the end.

    jbox_start --cron_log true --backend_port port_number &

### jbox_start_back_end
If you only want to run the backend for serving the db and front-end without having to start the cron again.

    jbox_start_back_end --backend_port port_number

### jbox_start_cron
The command will start the cron job that runs every minute to parse the jupyter notebooks. To run on the background add the `&` at the end.

    jbox_start_cron &

## How to convert the notebook to be used in JBox
As said before, to get a notebook used into JBox you need to add comments into the cells you want to convert. There are 2 functions that are used, one for converting and the second one for installing needed packages for the notebook. Every comment used will start the following way (remmeber the space between the # and jbox):

    # jbox <commands>

### Installing Packages
In order to have the packages the notebooks need you can run the following line at the top of a cell. This is normally done at the first cell on the beginning of the notebook. 

    # jbox installations <library_names>

You can have multiple libary names separated by commas.

    # jbox installations pandas numpy ipydatatable

### Converting a cell into a block.
In order to convert a cell in a notebook into block in JBox there are multiple options. We will go over all the options availble and how to use them. Below you have all the options available.

    # jbox <block_name>
        -inputs <array_of_input_names> 
        -outputs <array_of_output_names> 
        -description <description_of_block>
        -pydoc 
        -def

#### Cell with no inputs and outputs
The most generic cell, with no inputs, outputs or a defined function.

```python
# jbox hello_world -inputs [] -outputs [] -description 'Function to print Hello World!'
print("Hello World!")
```

#### Cell with inputs and outputs 
In this case you will have a cell with variables been used on the code that can be used as inputs and also variables that will be used as an output.
```python
# jbox input_world -inputs [a,b] -outputs [c] -description 'Function with 2 inputs and one output.'

c = str(a)+" "+str(b)
```
In this example the inputs are `a` and `b` and the output will be the `c`.

#### Defined function block
In this case we are using a function that is already created and it already has a return. This one also has a pydoc used for the description of the block.
```python
# jbox output_only_function -inputs [] -outputs [c,d] -pydoc -def

def output_only_function():
    '''
        Function to show off the using a defined function and a 
        pydoc as the descriotion
    '''
    c = "Hello"
    d = "World"
    return [c,d]
```