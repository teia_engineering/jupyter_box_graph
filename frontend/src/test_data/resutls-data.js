export const results = {
    "error": false, 
    "logs": "Nothing printed in here.", 
    "output": {
      "c": "Hello", 
      "d": "World"
    }
  }

export const error = {
    "comment": "Inputs required for this box.",
    "error": true
}