import React from 'react';

// text == block name
export default [
  {
    id: '2',
    type: '4eb49d8e-61db-11ec-9265-f018984fdf58',
    position: { x: 100, y: 100 },
    data: { 
      text: 'A custom node',
      'inputs': ["name1", "name2"],
      'outputs': ["name3","name4"],
      'block_type': "4eb49d8e-61db-11ec-9265-f018984fdf58",
      'block_name': "name_to_display",
    },
  },
  {
    id: '3',
    type: '4',
    position: { x: 100, y: 100 },
    data: { 
      text: 'A custom node',
      'inputs': ["name1", "name2", "name8"],
      'outputs': ["name3","name4"],
      'block_type': "name_on_backend",
      'block_name': "name_to_display_2",
    },
  },
  {
    id: '4',
    type: 'name_on_backend',
    position: { x: 100, y: 100 },
    data: { 
      text: 'A custom node',
      'inputs': ["name1", "name2", "name8"],
      'outputs': ["name3","name4"],
      'block_type': "name_on_backend",
      'block_name': "name_to_display_4",
    },
  },
  {
    id: '5',
    type: 'name_on_backend',
    position: { x: 100, y: 100 },
    data: { 
      text: 'A custom node',
      'inputs': ["name1", "name2", "name8"],
      'outputs': ["name3","name4"],
      'block_type': "name_on_backend",
      'block_name': "name_to_display_5",
    },
  },
  {
    id: '6',
    type: 'name_on_backend',
    position: { x: 100, y: 100 },
    data: { 
      text: 'A custom node',
      'inputs': ["name1", "name2", "name8"],
      'outputs': ["name3","name4"],
      'block_type': "name_on_backend",
      'block_name': "name_to_display_6",
    },
  },
  {
    id: '7',
    type: 'name_on_backend',
    position: { x: 100, y: 100 },
    data: { 
      text: 'A custom node',
      'inputs': ["name1", "name2", "name8"],
      'outputs': ["name3","name4"],
      'block_type': "name_on_backend",
      'block_name': "name_to_display_7",
    },
  },
  { id: 'e2-3', source: '2', target: '3', targetHandle:"name2", sourceHandle:"name3"},
  { id: 'e2-4', source: '2', target: '4', targetHandle:"name2", sourceHandle:"name3"},
  { id: 'e4-5', source: '4', target: '5', targetHandle:"name2", sourceHandle:"name3"},
  { id: 'e6-7', source: '6', target: '7', targetHandle:"name2", sourceHandle:"name3"},
];