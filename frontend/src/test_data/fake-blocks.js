
export const Blocks = [
    {
        "block_id": "1",
        "code":"",
        "name": "Hello World",
        "description": "Hello World",
        "inputs": [],
        "outputs": [],
    },{
        "block_id": "2",
        "code":"",
        "name":"Block Test",
        "description": "testing block 2",
        "inputs": [],
        "outputs": [],
    },{
        "block_id": "3",
        "code":"",
        "name":"Block Test",
        "description": "testing block 3",
        "inputs": [],
        "outputs": [],
    },{
        "block_id": "4",
        "code":"",
        "name":"Block Test",
        "description": "testing block 4",
        "inputs": [],
        "outputs": [],
    },{
        "block_id": "5",
        "code":"",
        "name":"Block Test",
        "description": "testing block 5",
        "inputs": [],
        "outputs": [],
    },{
        "block_id": "6",
        "code":"",
        "name":"Block Test",
        "description": "testing block 6",
        "inputs": [],
        "outputs": [],
    },{
        "block_id": "7",
        "code":"",
        "name":"Block Test",
        "description": "testing block 7",
        "inputs": [],
        "outputs": [],
    },
];