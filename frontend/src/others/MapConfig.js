export const maps = {
    "tileLayers":[
        {
            "name":"OpenStreetMap.Mapnik",
            "url":"https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            "attribution":'&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
            "params":{},
            "checked":true
        },
        {
            "name":"OpenStreetMap.BlackAndWhite",
            "url":"https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            "attribution":'amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
            "params":{},
            "checked":false
        }
    ],
    "wmsTileLayers":[
        {
            "name":"WMS Hillshade",
            "url":"http://ows.mundialis.de/services/service?",
            "attribution":"",
            "layers":"SRTM30-Colored-Hillshade",
            "checked":false
        }
    ]
}