export const genericBlocks = {
    "input0": {
      description: "Function that allows a user to add text inputs.",
      file_name: null,
      inputs: "[]",
      name: "Text Inputs",
      outputs: "[text_input]",
      original_file: "Input Blocks",
      type: "input",
      extra:'text'
    },
    "input1": {
      description: "Function that allows a user to add a file as input and passes that file as an output.",
      file_name: null,
      inputs: "[]",
      name: "File Inputs",
      outputs: "[file_path]",
      original_file: "Input Blocks",
      type: "input",
      extra:'file'
    },
    "input2": {
      description: "Function that allows a user to add a multi line text inputs.",
      file_name: null,
      inputs: "[]",
      name: "Multi Line Text Inputs",
      outputs: "[text_input]",
      original_file: "Input Blocks",
      type: "input",
      extra:'multi_text'
    },
    "output0": {
      description: "Display Interactive Table from JSON.",
      file_name: null,
      inputs: "[table_input]",
      name: "Interactive Table",
      outputs: "[]",
      original_file: "Output Blocks",
      type: "output",
      extra:'table'
    },
    "output2": {
      description: "Display Data in Map",
      file_name: null,
      inputs: "[map_data]",
      name: "Interactive Map",
      outputs: "[]",
      original_file: "Output Blocks",
      type: "output",
      extra:'map'
    },
    "output3": {
      description: "Display Data in Several Types of Graphs",
      file_name: null,
      inputs: "[graph_data]",
      name: "Interactive Graph",
      outputs: "[]",
      original_file: "Output Blocks",
      type: "output",
      extra:'graphs'
    },
  }