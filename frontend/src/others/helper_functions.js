import dagre from 'dagre';

import {
    isNode,
  } from 'reactflow';
// Node sizes
const nodeWidth = 172;
const nodeHeight = 36;

let dagreGraph = new dagre.graphlib.Graph();
dagreGraph.setDefaultEdgeLabel(() => ({}));

// Using the dagre library to set the node locations.
export const getLayoutedElements = (elements, direction = 'TB') => {    
    const isHorizontal = direction === 'LR';
    dagreGraph.setGraph({ rankdir: direction });
  
    elements.forEach((el) => {
      if (isNode(el)) {
        dagreGraph.setNode(el.id, { width: nodeWidth, height: nodeHeight });
      } else {
        dagreGraph.setEdge(el.source, el.target);
      }
    });
  
    dagre.layout(dagreGraph);
  
    return elements.map((el) => {
      if (isNode(el)) {
        const nodeWithPosition = dagreGraph.node(el.id);
        el.targetPosition = isHorizontal ? 'left' : 'top';
        el.sourcePosition = isHorizontal ? 'right' : 'bottom';
  
        // unfortunately we need this little hack to pass a slightly different position
        // to notify react flow about the change. Moreover we are shifting the dagre node position
        // (anchor=center center) to the top left so it matches the react flow node anchor point (top left).
        el.position = {
          x: nodeWithPosition.x - nodeWidth / 2 + Math.random() / 1000,
          y: nodeWithPosition.y - nodeHeight / 2,
        };
      }
  
      return el;
    });
  };

export const getEdges = (nodeId, all_data, outputs) => {
    let edges = {};
    let box_edge = {};

    all_data.forEach((element) => {
      if(element.target === nodeId){
          if(Object.keys(box_edge).includes(element.source)){
              box_edge[element.source].push(element);
          }
          else{
              box_edge[element.source] = [element]
          }
          
      }
    });

    Object.keys(box_edge).forEach((element) => {
        box_edge[element].forEach((value) => {
            edges[value['targetHandle']] = outputs[element]['result']['output'][value['sourceHandle']]
        })
        
    })

    return edges;
}

export const removeNodes = (nodeId, all_data) => {
    let temp = []

    all_data.forEach((element) => {
        if(typeof element.id !== "number" && element.id.includes("edge") && ""+element.target !== ""+nodeId && ""+element.source !== ""+nodeId){
            temp.push(element)
        }
        else if(""+element.id !== ""+nodeId){
            temp.push(element)
        }
    })

    return temp
};