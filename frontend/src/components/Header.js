// Component for header section and buttons on navbar
import React, { useState } from 'react';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import {UploadWorkflowModal} from './modals/UploadModal'
import {UploadNotebook} from './modals/UploadNotebook'

export const Header = (props) => {
    // State Variables
    // Opening modal for uploading workflow file
    const [openUpload, setOpenUpload] = useState(false)
    // Opening modal for uploading notebook
    const [openUploadNotebook, setOpenUploadNotebook] = useState(false)

    // Function to download workflow file.
    const downloadFile = ({ data, fileName, fileType }) => {
        // Create a blob with the data we want to download as a file
        const blob = new Blob([data], { type: fileType })
        // Create an anchor element and dispatch a click event on it
        // to trigger a download
        const a = document.createElement('a')
        a.download = fileName
        a.href = window.URL.createObjectURL(blob)
        const clickEvt = new MouseEvent('click', {
          view: window,
          bubbles: true,
          cancelable: true,
        })
        a.dispatchEvent(clickEvt)
        a.remove()
    }
    
    // Call from selectiing button on navbar to download.
    const exportToJson = e => {
        let data = {
            'data':props.data,
            'outputs':props.outputs
        }
        downloadFile({
            data: JSON.stringify(data),
            fileName: 'data.json',
            fileType: 'text/json',
        })
    }

    // Handles selction of button with one function.
    const handleSelect = (selectedKey) => {
        if(selectedKey === "upload"){
            setOpenUpload(true)
        }
        else if(selectedKey === "download"){
            exportToJson();
        }
        else if(selectedKey === "uploadNotebook"){
            setOpenUploadNotebook(true);
        }
    }
    return (
        <>
            <Navbar bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand href="#">Jupyter Boxes</Navbar.Brand>
                    <Nav className="me-auto"  onSelect={handleSelect} >
                        <Nav.Link eventKey={"uploadNotebook"} href="#uploadNotebook">Upload Notebook</Nav.Link>
                        <Nav.Link eventKey={"upload"} href="#uploadData">Upload Workflow</Nav.Link>
                        <Nav.Link eventKey={"download"} href="#download">Download Workflow</Nav.Link>
                        <Nav.Link eventKey={"help"} href="#help">Help</Nav.Link>
                        <Nav.Link eventKey={"about"} href="#about">About</Nav.Link>
                    </Nav>
                </Container>
            </Navbar>
            <UploadWorkflowModal show={openUpload} 
                                 close={setOpenUpload} 
                                 setOutputs={props.setOutputs}
                                 setUploadWorkdlow={props.setUploadWorkdlow} 
                                 setData={props.setData} />

            <UploadNotebook show={openUploadNotebook} 
                            close={setOpenUploadNotebook} 
                            url={props.url}
                            parsingNotebooks={props.parsingNotebooks}
                            setParsingNotebooks={props.setParsingNotebooks} />
        </>
    )
}
