// Component containing all the squares in the react flow renderer.
// Gets called from the App.js
import React, { useEffect, useState, useCallback } from 'react';

import ReactFlow, {
  addEdge,
  MiniMap,
  Controls,
  Background,
  // ControlButton,
  useNodesState,
  useEdgesState,
  getIncomers,
  getOutgoers,
  getConnectedEdges,
  //useZoomPanHelper,
} from "reactflow";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faProjectDiagram } from '@fortawesome/free-solid-svg-icons'
import {GenerateBlockType} from './CreateNode';
import { getEdges, removeNodes, 
  // getLayoutedElements 
} from '../others/helper_functions';
import {TableModal} from './modals/TableModal';
import { MapModal } from './modals/MapModal';
import {ResultsModal} from './modals/ResultsModal';
import {D3Graph} from './modals/D3';

import "reactflow/dist/style.css";

// Node template used to store the data in the backend.
const template = {
  id: 0,
  type: 'name_on_backend',
  position: { x: 100, y: 100 },
  data: { 
    text: 'A custom node',
    'inputs': ["name1", "name2", "name8"],
    'outputs': ["name3","name4"],
    'block_type': "name_on_backend",
    'block_name': "name_to_display_7",
    'color':"#9CA8B3",
    'extra':"",
    'id':0,
  },
}

// Component Code
export const BlockArea = (props) => {
  //const { fitView } = useZoomPanHelper();
  // State Variables.
  // All the elements in the graph before dagre positions
  // const [elements, setElements] = useState([]);
  const [nodes, setNodes, onNodesChange] = useNodesState([]);
  const [edges, setEdges, onEdgesChange] = useEdgesState([]);
  // From running the nodes on the graph.
  const [outputs, setOutputs] = useState({});
  // Creating Node types for nodes. Since our nodes are
  // not a default node, we need ot create them for 
  // react flow renderer.
  const [tNodeTypes, setTNodeTypes] = useState({});

  // Contains the information to display modal from blocks.
  const [ modalFeatures, setModalFeatures ] = useState({type:""});

  // opening results modal
  const [openResultsModal, setOpenResultsModal] = useState(false);

  // Removing nodes after trash clicked.
  const removeNodeClicked = (nodeId) => {
    nodes.forEach((value)=> {
      if(value['id'] === nodeId){
        onNodesDelete([value])
        return
      }
    })
    setNodes((els)=>{
      let temp = removeNodes(nodeId, els)
      return temp;
    })


    let t = props.data
    t['nodes'] = [...nodes]
    props.setData({...t})
  }
    
  // Converting the input/output format names of
  // the blocks. format is [in1,in2], etc.
  const convertInputOutput = (data)=> {
    let return_data = []
    let t = data.replace("[","").replace("]","")
    if (t.includes(",") || t !== ""){
      return_data = t.split(",");
    }
    return return_data;
  }

  // Function to set color for blocks.
  const setColor = (id, color) => {
    let tGraph = []
    // Setting the color in the elments
    nodes.forEach((element, index)=>{
        if(element['id'] === ""+id || element['id'] === id){
          let t = {...element};
          t['data']['color'] = color
          tGraph.push(t)
        }
        else{
          tGraph.push({...element});
        }
    })

    let t = props.data
    t['nodes'] = [...tGraph]
    props.setData({...t})
  }

  const openModal = () =>{
    setOpenResultsModal(!openResultsModal)
  }

  // Function to get inputs depending on the block running.
  // Passing to getEdges (helper_functions.js) all the elements 
  // and the outputs generated from each of the runs.
  const getInputs = (id_t) => {
    return getEdges(id_t, edges, outputs)
  }

  const updatingNodes = () =>{
    let tempTypes = {}
    Object.keys(props.blocks).forEach((v,k) => {      
      tempTypes[v] = ({ data }) => {
        return GenerateBlockType(data, setColor, setOutputs, outputs, getInputs, removeNodeClicked, props.backend_port, setModalFeatures, setOpenResultsModal)
      };        
    })
    
    return tempTypes;
  }

  useEffect(()=>{
    setTNodeTypes(updatingNodes())
  },[edges])

  // Saving state of nodes when moving them.
  const nodeDrag = (event, node) =>{
    let tGraph = []
    
    node['id'] = node['id'].toString();

    nodes.forEach((element, index)=>{
      element['id'] = element['id'].toString();
      if(element['id'] === node['id']){
        tGraph.push(node)
      }
      else{
        tGraph.push(element)
      }
    })

    setNodes(tGraph)
    let t = props.data
    t['nodes'] = [...nodes]
    props.setData({...t})
  }

  // const formatGraph = () => {
  //   setElements(getLayoutedElements(elements,"LR"));
    
  // }

  // Updating graph wiht new data nodes.
  useEffect(() => {
    let temp = {...outputs};
    temp[props.newData['block_id']] = props.newOutput;
    
    setOutputs(temp)
  }, [props.newOutput])

  // Updating graph wiht new data nodes.
  useEffect(() => {
    // setElements(props.data);
    if(props.uploadWorkflow){
      template['id'] = parseInt(props.data['nodes'][props.data['nodes'].length-1])
      setNodes(props.data['nodes'])
      setEdges(props.data['edges'])
      setOutputs(props.allOutputs)
      props.setUploadWorkdlow(false)
    }
  }, [props.uploadWorkflow])

  // In case there are new blocks in the server
  // create new node types.
  useEffect(() => {

    setTNodeTypes(updatingNodes())
    setOutputs(outputs);
  }, [props.blocks, outputs])

  // useEffect(() =>{
  //   setOutputs(props.allOutput)
  // }, [props.allOutput])

  // Creating node from the selected side bar. Generating the 
  // new one using the template. 
  useEffect(() => {      
    if(Object.keys(props.newData).length > 0){
      let block_info = props.blocks[props.newData["block_id"]]
      template['id'] = template['id']+1;
      let tTemplate = {...template};
      tTemplate['id'] = "" + tTemplate['id'];
      tTemplate['type'] = props.newData['block_id'];
      tTemplate['data'] = {...tTemplate['data']}
      tTemplate['data']['id'] = ""+tTemplate['id'];
      tTemplate['data']['inputs'] = convertInputOutput(block_info["inputs"]);
      tTemplate['data']['outputs'] = convertInputOutput(block_info["outputs"]); 
      tTemplate['data']['block_type'] = props.newData["block_id"];
      tTemplate['data']['text'] = props.newData['name']
      tTemplate['data']['block_name'] = props.newData['name'];
      tTemplate['data']['extra'] = block_info['extra'];
      nodes.push(tTemplate)
      let tElem = [...nodes];
      setNodes([...nodes])
      // setElements(elements)

      if(Object.keys(props.newData).includes("input_output")){
        let temp = {...outputs};
        temp[tTemplate['id']] = props.newData['input_output'];
        
        setOutputs(temp)
      }
      let t = props.data
      t['nodes'] = [...nodes]
      props.setData({...t})
    }
  },[props.newData]);


  const onConnect = useCallback((params) => setEdges((eds) => addEdge(params, eds)), []);

  const onNodesDelete = useCallback(
    (deleted) => {
      setEdges(
        deleted.reduce((acc, node) => {
          const incomers = getIncomers(node, nodes, edges);
          const outgoers = getOutgoers(node, nodes, edges);
          const connectedEdges = getConnectedEdges([node], edges);

          const remainingEdges = acc.filter((edge) => !connectedEdges.includes(edge));

          const createdEdges = incomers.flatMap(({ id: source }) =>
            outgoers.map(({ id: target }) => ({ id: `${source}->${target}`, source, target }))
          );

          return [...remainingEdges, ...createdEdges];
        }, edges)
      );
    },
    [nodes, edges]
  );

  useEffect(() =>{
    let t = props.data
    t['edges'] = [...edges]
    props.setData({...t})
  }, [edges])

  return (
      <div style={{ height: 600, borderStyle: "solid", borderWidth: "1px" }}>
          <ReactFlow
              nodes={nodes}
              edges={edges}
              onConnect={onConnect}
              onNodesChange={onNodesChange}
              onEdgesChange={onEdgesChange}
              onNodesDelete={onNodesDelete}
              onNodeDragStop={nodeDrag}
              nodeTypes={tNodeTypes}
              snapGrid={[15, 15]}
              attributionPosition="top-right"
          >
          <MiniMap
          nodeStrokeColor={(n) => {
              if (n.style?.background) return n.style.background;
              if (n.type === 'input') return '#0041d0';
              if (n.type === 'output') return '#ff0072';
              if (n.type === 'default') return '#1a192b';
  
              return '#eee';
          }}
          nodeColor={(n) => {
              if (n.style?.background) return n.style.background;
  
              return '#fff';
          }}
          nodeBorderRadius={2}
          />
          <Controls>
          {/* <ControlButton onClick={() => formatGraph()}>
            <FontAwesomeIcon id={"control"} icon={faProjectDiagram} />
          </ControlButton> */}
          </Controls>
          <Background color="#aaa" gap={16} />
      </ReactFlow>
      { modalFeatures['type'] === "tableModal" && 
        <TableModal name={modalFeatures['name']} 
                    show={openResultsModal} 
                    close={openModal} 
                    results={modalFeatures['results']} 
                    id={modalFeatures['id']} />
      }
      { modalFeatures['type'] === "mapModal" && 
        <MapModal name={modalFeatures['name']} 
                    show={openResultsModal} 
                    close={openModal} 
                    results={modalFeatures['results']} 
                    id={modalFeatures['id']} />
      }
      { modalFeatures['type'] === "graphModal" && 
        <D3Graph name={modalFeatures['name']} 
          show={openResultsModal} 
          close={openModal} 
          results={modalFeatures['results']} 
          id={modalFeatures['id']} />
      }
      { modalFeatures['type'] === "resultsModal" && 
        <ResultsModal name={modalFeatures['name']} 
          show={openResultsModal} 
          close={openModal} 
          results={modalFeatures['results']} 
          id={modalFeatures['id']} />
      }
      
    </div>
  )
}