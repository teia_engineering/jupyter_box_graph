// Component to create a Square node in the react flow renderer.
// Called from BlockArea.js
import { Handle } from 'reactflow';
import { Row, Col } from 'react-bootstrap';
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import Spinner from "react-bootstrap/Spinner";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay, faTrash, faTasks } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import { useState, useEffect } from 'react';

/**
 * JSON object for a node shall be something like the following:
 * {
 *  'inputs': ["name1", "name2"],
 *  'outputs': ["name3","name4"],
 *  'block_type': "name_on_backend",
 *  'block_name': "name_to_display",
 * }
 */

// Calculates heighn of box depending on the amount of inputs
// and ouputs.
const box_height_calculations = (total, differential) =>{
    let divisable = (100 - differential)/total;
    let returnable_values = [];
    for (let i = 0; i < total; i++) {
        returnable_values.push(i*divisable+differential)
    }

    return returnable_values;
}

// Component code
export const GenerateBlockType = (block_info, setColor, setOutputs, outputs, getInputs, removeNodeClicked, backend_port, setModalFeatures, setOpenResultsModal) => {
    // State variables
    // Loading gif when the user hits run.
    const [loadingQuery, setLoadingQuery] = useState(false);
    // Opening the modal with the results
    // Node style, this includes color change.
    let customNodeStyles = {
        background: block_info['color'],
        color: '#FFF',
        fontSize: "5px",
        padding:"3px",
        borderRadius: "2px",
    };

    // Check for change in outputs.
    useEffect(() => {
        
        
    }, [outputs])

    const checkStatus = (data) => {
        axios
            .post(backend_port+"/jbox_rest/task_status", {
                "job_id":data.data['result']['output']['job_id'],
            }).then(data2 =>{
                    if(data2.data['status'] === "SUCCESS" || data2.data['status'] === "FAILURE"){
                        let temp = {...outputs};
                        temp[block_info['id']] = data2.data;
                        setOutputs(temp)
                    }
                    else{
                        setTimeout(function() { //Start the timer
                            checkStatus(data);
                        }, 5000)
                        console.log("Still Waiting for server to complete....")
                    }
                }
            )
    }

    // Function to run the block when button gets clicked.
    const runBlock = () => {
        console.log("running block");
        
        // Start loading gif
        setLoadingQuery(true);

        // Getting inputs associated to other blocks. (BlockArea.js)
        let inputsDict = getInputs(block_info['id']);
        let inputs = [];
        block_info['inputs'].forEach(element => {
            inputs.push(inputsDict[element]);
        });
        if( block_info['block_type'].includes("input")){
            setLoadingQuery(false);
        }
        else{
            axios
            .post(backend_port+"/jbox_rest/execute_box", {
                "id":block_info['block_type'],
                "inputs": inputs,
            })
            .then(data => {
                if(data.data['status'] === "SUCCESS" && !data.data['result']['output'].hasOwnProperty("job_id")){
                    let temp = {...outputs};
                    temp[block_info['id']] = data.data;
                    setOutputs(temp)
                }
                else{
                    checkStatus(data);
                }
            })
            .catch(error => {
                setLoadingQuery(false);
            });
        }
    }

    const colorChange = (color) => {
        let newColor = color.target.value;

        setColor(block_info['id'], newColor);
    }

    const openModal = () =>{
        let type ="";
        if(block_info['block_type'].includes("output") && block_info['extra'] === "table"){
            type="tableModal"
        }
        else if(block_info['block_type'].includes("output") && block_info['extra'] === "map"){
            type="mapModal"
        }
        else if(block_info['block_type'].includes("output") && block_info['extra'] === "graphs"){
            type="graphModal"
        }
        else if(!block_info['block_type'].includes("output")){
            type="resultsModal";
        }
        let features = {
            "type": type,
            "name": block_info['block_name'],
            "results": outputs,
            "id": ""+block_info['id']
        }
        setModalFeatures(features)
        setOpenResultsModal(true)
        
    }

    const generateHandle = (type, position, name, top) => {
        const renderTooltip = props => (
            <Tooltip key={name} {...props}>{name}</Tooltip>
        );
        
        return (
            <OverlayTrigger key={name+"_"+block_info['id']} placement="top" overlay={renderTooltip}>
                <Handle
                    type={type}
                    position={position}
                    id={name}
                    key={name}
                    style={{ top: top+'%', borderRadius: 0, background: '#000'  }}
                    onConnect={(params) => console.log('handle onConnect', params)}
                >   
                </Handle>
            </OverlayTrigger>
        )
    }

    const handleDelete = (e) => {
        removeNodeClicked(e.currentTarget.id)
        //console.log(e.currentTarget.id)
    }

    let input_height = box_height_calculations(block_info.inputs.length,(100/block_info.inputs.length)/2);
    let output_height = box_height_calculations(block_info.outputs.length,(100/block_info.outputs.length)/2);
    return (
        <div style={customNodeStyles} key={""+block_info['id']}>
            {
                block_info.inputs.map((val, i) =>{
                    return generateHandle("target","left",val, input_height[i]);
                })
            }

            <Row>
                <Col>
                    <div>
                        { loadingQuery &&
                            <Spinner animation="border" variant="light" style={{width:"5px", height:"5px"}} />
                        }
                        
                        { !loadingQuery &&
                            <button onClick={runBlock} style={{"fontSize":"5px", 
                                padding:"0", 
                                color:"white", 
                                backgroundColor: "rgba(201, 76, 76, 0.0)",
                                borderColor: "rgba(201, 76, 76, 0.0)",
                                }} ><FontAwesomeIcon icon={faPlay} /></button>
                        }
                        
                    </div>
                    
                </Col>
                <Col>
                    <input
                        className="nodrag"
                        style={{"height":"10px", width:"10px", border:"1px"}}
                        type="color"
                        onBlur={colorChange}
                        defaultValue={block_info['color']}
                    />
                </Col>
                <Col>
                    <div>
                        <button id={block_info['id']} onClick={handleDelete} style={{"fontSize":"5px", 
                                        padding:"0", 
                                        color:"white", 
                                        backgroundColor: "rgba(201, 76, 76, 0.0)",
                                        borderColor: "rgba(201, 76, 76, 0.0)",
                                        }} ><FontAwesomeIcon id={block_info} icon={faTrash} /></button>
                    </div>
                </Col>
                
                
            </Row>
            <Row>
                { Object.keys(outputs).includes(""+block_info['id'])  &&
                    <button onClick={openModal} style={{"fontSize":"5px", 
                        padding:"0", 
                        color:"white", 
                        backgroundColor: "rgba(201, 76, 76, 0.0)",
                        borderColor: "rgba(201, 76, 76, 0.0)",
                        }} ><FontAwesomeIcon icon={faTasks} /></button>
                }
            </Row>
            <Row>
                <div style={{"fontSize":"10px"}}>{block_info.block_name} </div>
            </Row>

            
            
            {
                block_info.outputs.map((val, i) =>{
                    return generateHandle("source","right",val, output_height[i]);
                })
            }
            
        </div>
    )
}