import React, { useState, useEffect } from 'react';
import Accordion from 'react-bootstrap/Accordion';
import { AccordionComponenet } from './Accordions';
import axios from 'axios';

var timer = null;
// Component that creates the sidebar with the creation boxes.
// Gets called from App.js
export const BlockSideBar = (props) => {
    const [filter, setFilter] = useState('');
    const [accordion, setAccordions] = useState({"accordions":{},"loading":[]})

    useEffect(()=>{
        let tAccordion = {"accordions":{},"loading":[]}
        let hits = []
        let loading = [];
        let files = props.parsingNotebooks['files'];
        if(typeof files === 'undefined') {
            files = []
        }
        Object.keys(props.blocks).forEach(val => {
            let acc_val = props.blocks[val]['original_file'].split(".ipynb")[0]
            if(!(acc_val in tAccordion["accordions"])){
                tAccordion["accordions"][acc_val] = {}
            }
            tAccordion["accordions"][acc_val][val] = props.blocks[val]
            if(acc_val+".ipynb" in files){
                tAccordion["loading"].push(loading)
                hits.push(acc_val+".ipynb")
            }
        })

        files.forEach((value) => {
            if(!(value in hits)){
                tAccordion["loading"].push(value.split(".ipynb")[0])
                tAccordion["accordions"][value.split(".ipynb")[0]] = {}
            }
        })

        setAccordions(tAccordion)
    },[props.blocks, props.parsingNotebooks])

    useEffect(() => {
        const getParsingNotebooks = () =>{
            axios
                .get(props.url+"/jbox_rest/check_nb_parsing")
                .then(data => {
                    if("files" in data['data'] && data['data']['files'].length > 0){
                        console.log("still parsing")
                    }
                    else{
                        clearInterval(timer)
                        props.setParsingNotebooks({})
                    }
                })
                .catch(error => console.log(error));
        }
        if(Object.keys(props.parsingNotebooks).length !== 0 ){
            timer = setInterval(() => {
                getParsingNotebooks();
            },5*1000);
            return () => clearInterval(timer);
        }
    }, [props]);
    
    return (
        <div style={{ height: 600, borderStyle: "solid", borderWidth: "1px", overflowY: "auto"}}>
            <div style={{padding: "5px", width:"100%"}}>
                <input id="filter"
                    name="filter"
                    type="text"
                    value={filter}
                    onChange={event => setFilter(event.target.value)}
                    placeholder="Search Boxes..."  
                    style={{width:"100%"}}
                />
            </div>
            
            <div style={{padding: "5px"}}>
                <Accordion alwaysOpen>
                    {
                        Object.keys(accordion['accordions']).map((k,v)=>{
                            return <AccordionComponenet 
                                key={k}
                                accordionTitle={k} 
                                blocks={accordion["accordions"][k]} 
                                loading={accordion["loading"]}
                                filter={filter}
                                updateBlock={props.updateBlock}
                                url={props.url} />
                        })
                    }
                </Accordion>
            </div>
        </div>
    )
}