import React, { useState } from 'react';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

export const MultiTextInputModal = (props) => {
    const [blockName, setBlockName] = useState("")
    const [inputText, setInputText] = useState("")
    const outputDict = {"children":[],"result":{"logs":[],"output":{}},"status":"SUCCESS","task_id":"","traceback":null};
    const addBlock = () =>{
        outputDict['result']['logs'] = "Input Text: "+inputText
        outputDict['result']['output']['text_input'] = inputText
        props.updateBlock({"name":blockName, "block_id": props.block_id, "input_output":outputDict})
        props.close();
    }

    return (
        <Modal show={props.show} onHide={props.close}>
            <Modal.Header closeButton>
                <Modal.Title>Adding "{props.name}" Block</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Form.Group className="mb-3" controlId="formBlockName">
                    <Form.Label>Block Name</Form.Label>
                    <Form.Control 
                        type="textarea" 
                        placeholder="Block Name" 
                        onChange={e => setBlockName(e.target.value)}
                    />
                    
                </Form.Group>
                <Form.Group className="mb-3" controlId="formInputText">
                    <Form.Label>Input Value</Form.Label>
                    <Form.Control 
                        type="textarea" 
                        as="textarea"
                        rows={5}
                        placeholder="Input Value" 
                        onChange={e => setInputText(e.target.value)}
                    />
                    
                </Form.Group>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={props.close}>Close</Button>
                <Button variant="primary" onClick={addBlock}>Add</Button>
            </Modal.Footer>
        </Modal>
    )
}