import React, { useEffect } from 'react'
import { useMap } from 'react-leaflet'

export const MapRefresh = (props) => {
    const map = useMap();

    useEffect(()=>{
        props.setMap(map)
    }, [props, map])

    return (
        <></>
    )
}