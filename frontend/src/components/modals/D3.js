import React, { useState, useEffect } from 'react'
// import { BarChart } from './D3js/BarChart';
import { FDG } from './D3js/FDG';
import Modal from 'react-bootstrap/Modal';
import Accordion from 'react-bootstrap/Accordion';
import Form from 'react-bootstrap/Form';

export const D3Graph = (props) => {
    const [data, setData] = useState({});
    const [type, setType] = useState("");
    
    const GRAPH_TYPES = ['forceDirected']

    useEffect(()=>{
      if(props.id in props.results && props.results[props.id]['result']['output']['output'].length > 0){
        setData(props.results[props.id]['result']['output']['output'][0])
      }
    },[props.results, props.id])
    return (
        <>
          <Modal show={props.show} onHide={props.close} size="lg">
            <Modal.Header closeButton>
                <Modal.Title>Graph Output</Modal.Title>
            </Modal.Header>

            <Modal.Body >
              <Accordion defaultActiveKey="options" alwaysOpen>
                <Accordion.Item eventKey="options">
                  <Accordion.Header>Graph Options</Accordion.Header>
                  <Accordion.Body>
                    <Form.Group className="mb-2" controlId="lat">
                      <Form.Label>Select Graph Type:</Form.Label>
                      <Form.Select aria-label="Graph type"
                        onChange={(val) => { setType(val.target.value) }}>
                        <option value={""}>Select</option>
                        {
                          GRAPH_TYPES.map((val,k) => {
                            return <option value={val}>{val}</option>
                          })
                        }
                      </Form.Select>
                    </Form.Group>
                  </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="graphs">
                  <Accordion.Header>Graph</Accordion.Header>
                  <Accordion.Body>
                    {/* <BarChart data={props.data} /> */}
                    { Object.keys(data).length > 0 && type === "forceDirected" &&
                      <FDG data={data} />
                    }
                    { Object.keys(data).length === 0 &&
                      <h1>No data on input. Please try again.</h1>
                    }
                    { type === "" &&
                      <h1>Please Select a Graph Type Above.</h1>
                    }
                    
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>
              
            </Modal.Body>

            <Modal.Footer>
            </Modal.Footer>
          </Modal>
        </>
    )
}