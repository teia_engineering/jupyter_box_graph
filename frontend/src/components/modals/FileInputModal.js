import React, { useState } from 'react';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import axios from 'axios';

export const UploadFileInput = (props) => {
    const [selectedFile, setSelectedFile] = useState(null)
    const [fileUploaded, setFileUploaded] = useState({"upload":false, "comment":""})

    const [blockName, setBlockName] = useState("");
    const outputDict = {"children":[],"result":{"logs":[],"output":{}},"status":"SUCCESS","task_id":"","traceback":null};
    const addBlock = (output) =>{
        outputDict['result']['logs'] = "File path "+output
        outputDict['result']['output']['file_path'] = output
        props.updateBlock({"name":blockName, "block_id": props.block_id, "input_output":outputDict})
        props.close();
    }
    
    // On file select (from the pop up) 
    const onFileChange = event => { 
        // Update the state 
        setSelectedFile(event.target.files[0]); 
    }; 

    // On file upload (click the upload button) 
    const onFileUpload = () => { 
        // Create an object of formData 
        const formData = new FormData(); 
        try{
            // Update the formData object 
            formData.append( 
                "file", 
                selectedFile, 
                selectedFile.name 
            ); 

            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }
            
            // Request made to the backend api 
            // Send formData object 
            axios.post(props.url+"/jbox_rest/input_upload_file", formData, config)
                .then((response) => {
                    setSelectedFile(null)
                    setFileUploaded({upload:true, comment:response.data.result.logs.join(" ")})
                    if(response.data.status === "SUCCESS"){
                        addBlock(response.data.result.output.dir)
                    }
                }); 
        }catch(error){
            setFileUploaded({upload:true, comment:"Please select a new file before attempting to upload."})
        }
        
    }; 

    // File content to be displayed after 
    // file upload is complete 
    const fileData = () => { 
        if (selectedFile) { 
            
          return ( 
            <div> 
              <h4>File Details:</h4> 
              <p>File Name: {selectedFile.name}</p> 
              <p> 
                Last Modified:{" "} 
                {selectedFile.lastModifiedDate.toDateString()} 
              </p> 
            </div> 
          ); 
        }
        else if(fileUploaded.upload){
            return (
                <div> 
                    <p>{fileUploaded.comment}</p>
                </div> 
            );
        }
    }; 

    return (
        <Modal show={props.show} onHide={props.close}>
            <Modal.Header closeButton>
                <Modal.Title>Upload File</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Form.Group className="mb-3" controlId="formBlockName">
                    <Form.Label>Block Name</Form.Label>
                    <Form.Control 
                        type="textarea" 
                        placeholder="Block Name" 
                        onChange={e => setBlockName(e.target.value)}
                    />
                    
                </Form.Group>
                <input type="file" onChange={onFileChange} /> 
                <p>{fileData()}</p> 
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={props.close}>Close</Button>
                <Button variant="primary" onClick={onFileUpload}>Upload file</Button>
            </Modal.Footer>
        </Modal>
    )
}

