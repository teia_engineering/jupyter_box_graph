import React, { useState, useEffect } from 'react'
import { MapContainer, Marker, TileLayer, LayerGroup,
        LayersControl, WMSTileLayer, Popup } from 'react-leaflet'
import MarkerClusterGroup from 'react-leaflet-cluster'
import { MapRefresh } from './MapRefresh'
import Accordion from 'react-bootstrap/Accordion';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Multiselect from 'multiselect-react-dropdown';
import { maps } from '../../others/MapConfig.js'

import 'leaflet/dist/leaflet.css'

export const MapModal = (props) => {
  const [colums, setColumns] = useState([])
  const [allData, setAllData] = useState([])
  const [lat, setLatColumn] = useState("")
  const [lon, setLonColumn] = useState("")
  const [groupCol, setGroupCol] = useState("")
  const [popupCols, setPopupCols] = useState([])
  const [map, setMap] = useState(null);
  const RenderMap = () => {
    if(lat !== "" && lon !== ""){
      setTimeout(function(){ map.invalidateSize()}, 400);
    }
    return null;
  };

  const generateTileLayers = () =>{
    return maps["tileLayers"].map((value, index)=>{
      if(value['checked']){
        return <>
          <LayersControl.BaseLayer checked key={value['name']} name={value['name']}>
            <TileLayer
              {...value}
            />
          </LayersControl.BaseLayer>
        </>
      }
      return <>
        <LayersControl.BaseLayer key={value['name']} name={value['name']}>
          <TileLayer
            {...value}
          />
        </LayersControl.BaseLayer>
      </>
    })
  }

  const generateWMSTileLayers = () =>{
    return maps["wmsTileLayers"].map((value, index)=>{
      if(value['checked']){
        return <>
          <LayersControl.BaseLayer checked key={value['name']} name={value['name']}>
            <WMSTileLayer
              {...value}
            />
          </LayersControl.BaseLayer>
        </>
      }
      return <>
        <LayersControl.BaseLayer key={value['name']} name={value['name']}>
          <WMSTileLayer
            {...value}
          />
        </LayersControl.BaseLayer>
      </>
    })
  }

  const generateGroupLayers = () => {
    let tObject = {}
    allData.forEach((value)=>{
      if(!(value[groupCol] in tObject)){
        tObject[value[groupCol]] = [value];
      }
      else{
        tObject[value[groupCol]].push(value)
      }
    })
    let all = []
    Object.keys(tObject).forEach((value) => {
      all.push(
        <LayersControl.Overlay checked name={value}>
          <LayerGroup>
            <MarkerClusterGroup chunkedLoading>
            {tObject[value].map((row, index) => {
              return <>
                <Marker key={index} position={[row[lat], row[lon]]} title={row[lat]}>
                  <Popup>
                    { popupCols.length > 0 &&
                      popupCols.map((col,index) => {
                        return <><b>{col}</b>{':'} &nbsp; {row[col]}<br/></>
                      })
                    }
                  </Popup>
                </Marker>
              </>
            })}
            </MarkerClusterGroup>
          </LayerGroup>
        </LayersControl.Overlay>
      )
    })
    return all;
  }

  useEffect(()=>{
    const extractColumns = () => {
      let tCols = []
      if(props.id in props.results && props.results[props.id]['result']['output']['output'].length > 0){
        let tData = props.results[props.id]['result']['output']['output'][0];
        tData.forEach(element => {
          tCols = [...new Set([...tCols ,...Object.keys(element)])];
        });
      }
      setColumns(tCols);
    }
    extractColumns()
    if(props.id in props.results && props.results[props.id]['result']['output']['output'].length > 0){
      setAllData(props.results[props.id]['result']['output']['output'][0]);
    }
  }, [props.results, props.id])

  return (
    <>
      <Modal show={props.show} onHide={props.close} dialogClassName="table-modal-width" contentClassName="table-modal-height">
          <Modal.Header closeButton>
              <Modal.Title>Results for {props.name}</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Accordion defaultActiveKey="options" alwaysOpen>
              <Accordion.Item eventKey="options">
                <Accordion.Header>Map Options</Accordion.Header>
                <Accordion.Body>
                <Form>
                  <Row>
                    <Col md={"2"}>
                      <Form.Group className="mb-2" controlId="lat">
                        <Form.Label>Select Latitude:</Form.Label>
                        <Form.Select aria-label="Latitude Column"
                          onChange={(val) => { setLatColumn(val.target.value) }}>
                          <option key={""} value={""}>Select</option>
                          {
                            colums.map((val,k) => {
                              return <option key={val} value={val}>{val}</option>
                            })
                          }
                        </Form.Select>
                      </Form.Group>
                    </Col>
                    <Col md={"2"}>
                      <Form.Group className="mb-2" controlId="lon">
                        <Form.Label>Select Longitude:</Form.Label>
                        <Form.Select aria-label="Longitude Column" 
                          onChange={(val) => { setLonColumn(val.target.value) }}>
                          <option key={""} value={""}>Select</option>
                          {
                            colums.map((val,k) => {
                              return <option key={val} value={val}>{val}</option>
                            })
                          }
                        </Form.Select>
                      </Form.Group>
                    </Col>
                    <Col md={"2"}>
                      <Form.Group className="mb-2" controlId="lat">
                        <Form.Label>Select Group (optional):</Form.Label>
                        <Form.Select aria-label="Group Column"
                          onChange={(val) => { setGroupCol(val.target.value) }}>
                          <option key={""} value={""}>Select</option>
                          {
                            colums.map((val,k) => {
                              return <option key={val} value={val}>{val}</option>
                            })
                          }
                        </Form.Select>
                      </Form.Group>
                    </Col>
                    <Col md={"6"}>
                      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Select Popup Data (optional):</Form.Label>
                        <Multiselect
                          options={colums}
                          isObject={false}
                          avoidHighlightFirstOption={true}
                          onSelect={(val) => { setPopupCols([...val])}} 
                          onRemove={(val) => {setPopupCols([...val])}}
                          />

                      </Form.Group>
                    </Col>
                  </Row>
                </Form>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="map">
                <Accordion.Header onClick={RenderMap}>Show Map</Accordion.Header>
                <Accordion.Body>
                  { lat !== "" && lon !== "" &&
                    <div>
                      <MapContainer
                        style={{ height: '500px' }}
                        center={[1,1]}
                        zoom={4}
                        maxZoom={18}
                        scrollWheelZoom={true}
                      >
                        <MapRefresh setMap={setMap} />
                        <LayersControl position="topright">
                          { generateTileLayers() }
                          { generateWMSTileLayers() }
                          { groupCol !== "" &&
                            generateGroupLayers()
                          }
                        </LayersControl>
                        { groupCol === "" &&
                          <MarkerClusterGroup chunkedLoading>
                            {(allData).map((row, index) => (
                              <Marker key={index} position={[row[lat], row[lon]]} title={row[lat]}>
                                <Popup>
                                  { popupCols.length > 0 &&
                                    popupCols.map((col,index) => {
                                      return <><b>{col}</b>{':'} &nbsp; {row[col]}<br/></>
                                    })
                                  }
                                </Popup>
                              </Marker>
                            ))}
                          </MarkerClusterGroup>
                        }
                      </MapContainer>
                    </div>
                  }
                  { lat === "" && lon === "" &&
                    <span>Please select columns from above.</span>
                  }
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </Modal.Body>

          <Modal.Footer>
              <Button variant="secondary" onClick={props.close}>Close</Button>
          </Modal.Footer>
      </Modal>
    </>
    
  )
}