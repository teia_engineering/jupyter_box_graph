import React, { useState } from 'react';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import axios from 'axios';

export const UploadNotebook = (props) => {
    const [selectedFile, setSelectedFile] = useState(null)
    const [fileUploaded, setFileUploaded] = useState({"upload":false, "comment":""})
    
    // On file select (from the pop up) 
    const onFileChange = event => { 
        // Update the state 
        setSelectedFile(event.target.files[0]); 
    }; 

    // On file upload (click the upload button) 
    const onFileUpload = () => { 
        // Create an object of formData 
        const formData = new FormData(); 
        try{
            // Update the formData object 
            formData.append( 
                "file", 
                selectedFile, 
                selectedFile.name 
            ); 
            
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }
            
            // Request made to the backend api 
            // Send formData object 
            axios.post(props.url+"/jbox_rest/upload_notebook", formData, config)
                .then((response) => {
                    console.log("filename!!",selectedFile.name);
                    if("files" in props.parsingNotebooks){
                        props.parsingNotebooks['files'].push(selectedFile.name)
                    }
                    else{
                        props.parsingNotebooks['files'] = [selectedFile.name]
                    }
                    props.setParsingNotebooks({...props.parsingNotebooks})
                    setSelectedFile(null)
                    setFileUploaded({upload:true, comment:response.data.result.logs.join(" ")})
                }); 
        }catch(error){
            setFileUploaded({upload:true, comment:"Please select a new file before attempting to upload."})
        }
        
    }; 

    // File content to be displayed after 
    // file upload is complete 
    const fileData = () => { 
        if (selectedFile) { 
            
          return ( 
            <div> 
              <h4>File Details:</h4> 
              <p>File Name: {selectedFile.name}</p> 
            </div> 
          ); 
        }
        else if(fileUploaded.upload){
            return (
                <div> 
                    <p>{fileUploaded.comment}</p>
                </div> 
            );
        }
    }; 

    return (
        <Modal show={props.show} onHide={props.close}>
            <Modal.Header closeButton>
                <Modal.Title>Upload Notebook</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <input type="file" onChange={onFileChange} /> 
                <p>{fileData()}</p> 
            </Modal.Body>

            <Modal.Footer>
                <Button variant="primary" onClick={onFileUpload}>Upload NB</Button>
            </Modal.Footer>
        </Modal>
    )
}

