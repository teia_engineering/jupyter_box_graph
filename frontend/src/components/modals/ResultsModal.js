import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Accordion from 'react-bootstrap/Accordion';

export const ResultsModal = (props) => {
    const overflowStyle = {
        maxHeight: '200px', 
        overflow: 'auto',
    }

    const generateAccordion = (header, data) => {
        if(data === undefined){
            data = ""
        }
        return (
            <Accordion.Item eventKey={header}>
                <Accordion.Header>{header}</Accordion.Header>
                <Accordion.Body style={overflowStyle}>
                    <>
                        { typeof data === "object" &&
                            <div><pre>{JSON.stringify(data, null, 2) }</pre></div>
                        } 
                        { typeof data !== "object" &&
                            data
                        } 
                        
                    </>
                </Accordion.Body>
            </Accordion.Item>
        )
    }

    return (
        <Modal show={props.show} onHide={props.close}>
            <Modal.Header closeButton>
                <Modal.Title>Results for {props.name}</Modal.Title>
            </Modal.Header>

            <Modal.Body> 
                <Accordion defaultActiveKey="AccordionResults0">
                { Object.keys(props).includes("results") && Object.keys(props.results).includes(props.id) && props.results[props.id]['status'] === "FAILURE" && 
                    generateAccordion("Error Occurred",props.results[props.id]['traceback'])
                }
                {  Object.keys(props).includes("results") && Object.keys(props.results).includes(props.id) && !props.results[props.id]['error'] && 
                    generateAccordion("Logs",props.results[props.id]['result']['logs'])
                    
                }
                {  Object.keys(props).includes("results") && Object.keys(props.results).includes(props.id) && !props.results[props.id]['error'] &&  typeof props.results[props.id]['result'] === "object" &&
                    Object.keys(props.results[props.id]['result']['output']).map((value, key) => {
                        return generateAccordion("Output Variable: "+value, props.results[props.id]['result']['output'][value]);
                    })
                }
                {  Object.keys(props).includes("results") && Object.keys(props.results).includes(props.id) && !props.results[props.id]['error'] &&  typeof props.results[props.id]['result'] !== "object" &&
                    generateAccordion("Output", props.results[props.id]['result'])
                } 
                </Accordion>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={props.close}>Close</Button>
            </Modal.Footer>
        </Modal>
    )
}