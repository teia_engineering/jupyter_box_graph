import * as d3 from 'd3';
import { fa_codes } from './FACodes';
import React, { useEffect, useRef } from 'react'

export const FDG = (props) => {
    let ref = useRef();
    useEffect(() => {
        const ticked = () => {
            node.call(updateNode);
            link.call(updateLink);
        }

        const fixna = (x) => {
            if (isFinite(x)) return x;
            return 0;
        }

        const updateLink = (link) => {
            link.attr("x1", function(d) { return fixna(d.source.x); })
                .attr("y1", function(d) { return fixna(d.source.y); })
                .attr("x2", function(d) { return fixna(d.target.x); })
                .attr("y2", function(d) { return fixna(d.target.y); });
        }

        const updateNode = (node) => {
            node.attr("transform", (d) => {
                return "translate(" + fixna(d.x) + "," + fixna(d.y) + ")";
            });
        }

        const neigh = (a, b) => {
            return a === b || adjlist[a + "-" + b];
        }


        const focus = (d) => {
            var index = d3.select(d3.event.target).datum().id;

            if(hover_focus){
                node.style("opacity", function(o) {
                    return neigh(index, o.id) ? 1 : 0.1;
                });

                link.style("opacity", function(o) {
                    return o.source.id === index || o.target.id === index ? 1 : 0.1;
                });
            }
        }

        const unfocus = () => {
            if(hover_focus){
                node.style("opacity", 1);
                link.style("opacity", 1);
            }
        }

        const dragstarted = (d) => {
            hover_focus = true;
            d3.event.sourceEvent.stopPropagation();
            if (!d3.event.active) graphLayout.alphaTarget(0.3).restart();
            d.fx = d.x;
            d.fy = d.y;
        }

        const dragged = (d) => {
            d.fx = d3.event.x;
            d.fy = d3.event.y;
        }

        const dragended = (d) => {
            if (!d3.event.active) graphLayout.alphaTarget(0).stop();
            d.fx = null;
            d.fy = null;
        }


        const onClick = (d) => {
            focus();
        }

        const update_graph = () =>{
        let height = 780;
        let width = 500;
        graphLayout = d3.forceSimulation()
        .force("charge", d3.forceManyBody().strength(-250))
        .force("link", d3.forceLink().id(d => d.id).distance(200))
        .force("center", d3.forceCenter(width / 2, height / 2))
        .on("tick", ticked)

        // var valueline = d3.line()
        //   .x(function(d) { return d[0]; })
        //   .y(function(d) { return d[1]; })
        //   .curve(d3.curveCatmullRomClosed);

        var color = d3.scaleOrdinal(d3.schemeCategory10);
        var full_graph = { ...props.data };
        var graph = full_graph;

        // Make a shallow copy to protect against mutation, while
        // recycling old nodes to preserve position and velocity.
        const old = new Map(node.data().map(d => [d.id, d]));
        graph.nodes = graph.nodes.map(d => Object.assign(old.get(d.id) || {}, d));
        graph.links = graph.links.map(d => Object.assign({}, d));

        adjlist = [];

        graph.links.forEach((d, i) => {
            if(typeof d.source === 'object'){
                adjlist[d.source.id + "-" + d.target.id] = true;
                adjlist[d.target.id + "-" + d.source.id] = true;
            }
            else{
                adjlist[d.source + "-" + d.target] = true;
                adjlist[d.target + "-" + d.source] = true;
            }
        });

        link = link.data(graph.links, d => [d.source, d.target])
            .join("line")
            .attr("stroke", "#aaa") 
            .attr("stroke-width", "1px");

        node = node.data(graph.nodes, d => d.id)
            .join(enter => enter.append("g"))

        // NODES!!!
        node.append("circle")
            .on("click", onClick)
            .attr("r", (d) => {
            if(d.hasOwnProperty('radius')){
                return d.radius;
            }
            else{
                return 25;
            }
            })
            .attr("fill", (d) => {
            if(d.hasOwnProperty('color')){
                return d.color;
            }
            else{
                return color(Math.random());
            }
            })


        var label = true
        if(label){
            // LABELS!!!
            node.append("text")
            .attr("class","label_")
            .text(function(d) {
                if(d.hasOwnProperty('label')){
                return d.label;
                }
                else{
                return d.id;
                }
            })
            .attr('x', function(d) {
                if(d.hasOwnProperty('radius')){
                return d.radius;
                }
                else{
                return 25;
                }
            })
            .attr('y', function(d) {
                if(d.hasOwnProperty('radius')){
                return d.radius*-1;
                }
                else{
                return -25;
                }
            });
        }
        else{
            d3.selectAll(".label_").remove();
        }

        var icon = true;
        if(icon){
            // ICONS!!!
            node.append("text")
            .attr("class","icon_")
            .attr("class","fas fa-stack")
            .attr("x", 0)
            .attr("dy", ".35em")
            .attr("text-anchor", "middle")
            .style("fill", "white")
            .attr('font-family', 'FontAwesome')
            .attr('font-size', function(d) {
                if(d.hasOwnProperty('radius')){
                return d.radius
                } else {
                return 25;
                }
            } )
            .text(function(d) {
                return fa_codes[d.icon]
            });
        }
        else{
            d3.selectAll(".icon_").remove();
        }

        node.call(
            d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended)
        );

        node.on("mouseover", focus).on("mouseout", unfocus);

        graphLayout.nodes(graph.nodes);
        graphLayout.force("link").links(graph.links);

        graphLayout
            .alpha(1)
            .alphaTarget(0)

        graphLayout.restart();

        // See https://github.com/d3/d3-force/blob/master/README.md#simulation_tick
        for (var i = 0, n = Math.ceil(Math.log(graphLayout.alphaMin()) / Math.log(1 - graphLayout.alphaDecay())); i < n; ++i) {
            graphLayout.tick();
        }

        graphLayout.stop();

        node.call(updateNode);
        link.call(updateLink);


        }
        var hover_focus = true;
        var adjlist = [];
        var graphLayout = null;
        
        var zoom = d3.zoom()
                    .scaleExtent([.1, 10])
                    .on("zoom", () => { container.attr("transform", d3.event.transform); });

        var svg = null;
        var container = null;
        var link = null;
        var node = null;
        if(ref.current == null){
            svg = d3.select(".svg-canvas").append("svg").attr("width", "100%").attr("height", "500px").call(zoom);

            container = svg.append("g").attr("class", "topG");

            zoom.scaleTo(svg, .3);

            link = container.append("g").attr("class", "links")
            .selectAll("line");

            node = container.append("g").attr("class", "nodes")
            .selectAll("g");

            ref.current = svg
            update_graph();
        }
        

    },[props.data])


    return (
        <div>
            <div className="svg-canvas" >
            </div>
        </div>
        
    )
}