import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import React, {useState, useEffect} from 'react';
import { AgGridReact } from 'ag-grid-react';

export const TableModal = (props) => {
    const [table, setTable] = useState({cols:[], rows:[]});

    // Check for change in outputs.
    useEffect(() => {
      const generate_col_row = () => {
        let cols = [];
        if(props.id in props.results && props.results[props.id]['result']['output']['output'].length > 0){
            let json_obj = []
            try{
              json_obj = JSON.parse(props.results[props.id]['result']['output']['output'][0])
            }
            catch{
              json_obj = props.results[props.id]['result']['output']['output'][0]
            }
            
            if(json_obj !== undefined && json_obj !== null && json_obj.constructor === Array){
              let temp_cols = []
              json_obj.forEach((value, index)=>{
                for(let x in value){
                  if(!temp_cols.includes(x)){
                    cols.push({field:x, resizable: true, sortable: true})
                    temp_cols.push(x);
                  }
                }
              })
            }         
            else if(json_obj !== undefined && json_obj !== null && json_obj.constructor === Object){
                
                for(let x in json_obj){
                    cols.push({field:x, resizable: true, sortable: true})
                }
                json_obj = [json_obj]
            }    
            setTable({"cols":cols,"rows":json_obj})
        }
      }
      generate_col_row()
    }, [props,])
    
    return (
        <Modal show={props.show} onHide={props.close} dialogClassName="table-modal-width" contentClassName="table-modal-height">
            <Modal.Header closeButton>
                <Modal.Title>Results for {props.name}</Modal.Title>
            </Modal.Header>

            <Modal.Body>
            <div className="ag-theme-alpine" style={{height: window.innerHeight*.6+"px", width: "100%"}}>
              <AgGridReact
                  rowData={table['rows']}
                  columnDefs={table['cols']}>
              </AgGridReact>
            </div>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={props.close}>Close</Button>
            </Modal.Footer>
        </Modal>
    )
}