import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

export const AddingBlockModal = (props) => {
    const [blockName, setBlockName] = useState("")
    const addBlock = () =>{
        props.updateBlock({"name":blockName, "block_id": props.block_id})
        props.close();
    }

    return (
        <Modal show={props.show} onHide={props.close}>
            <Modal.Header closeButton>
                <Modal.Title>Adding "{props.name}" Block</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Form.Group className="mb-3" controlId="formBlockName">
                    <Form.Label>Block Name</Form.Label>
                    <Form.Control 
                        type="textarea" 
                        placeholder="Block Name" 
                        onChange={e => setBlockName(e.target.value)}
                    />
                    
                </Form.Group>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={props.close}>Close</Button>
                <Button variant="primary" onClick={addBlock}>Add</Button>
            </Modal.Footer>
        </Modal>
    )
}