import React, { useState } from 'react';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { Upload } from '../Upload'

export const UploadWorkflowModal = (props) => {
    const [data, setNewData] = useState([])
    const uploadData = () =>{
        props.setOutputs(data['outputs'])
        props.setData(data['data']);
        props.setUploadWorkdlow(true)
        props.close();
    }

    return (
        <Modal show={props.show} onHide={props.close}>
            <Modal.Header closeButton>
                <Modal.Title>Upload a JSON workflow</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Upload setFileData={setNewData}/>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="primary" onClick={uploadData}>upload</Button>
            </Modal.Footer>
        </Modal>
    )
}

