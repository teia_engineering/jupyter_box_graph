import React from 'react'; 

export const Upload = (props) => {
    let fileReader;
  
    const handleFileRead = (e) => {
        const content = fileReader.result;
        props.setFileData(JSON.parse(content))
    };
    
    const handleFileChosen = (file) => {
        fileReader = new FileReader();
        fileReader.onloadend = handleFileRead;
        fileReader.readAsText(file);
    };

    // On file select (from the pop up) 
    const onFileChange = event => { 
        handleFileChosen(event.target.files[0])
    }; 
       
    return ( 
        <div> 
            <div> 
                <input type="file" onChange={onFileChange} /> 
            </div> 
        </div> 
    ); 
}