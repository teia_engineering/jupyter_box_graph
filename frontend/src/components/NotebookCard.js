import React, { useState } from 'react';

import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { AddingBlockModal } from './modals/AddingBlock';
import { UploadFileInput } from './modals/FileInputModal';
import { TextInputModal } from './modals/TextInputModal';
import { MultiTextInputModal } from './modals/MultiTextInputModal';


// Component that creates the card on the scroll on the right
// Gets called in BlockSideBar
export const NotebookCard = (props) => {
    const [showModal, setShowModal] = useState(false)
    const openModal = () => {
        setShowModal(!showModal)
    }

    return (
        <>
            <Card style={{marginTop:"5px"}}>
                <Card.Header style={{background: 'orange'}}>
                    {props.name} 
                    <Button style={{float: 'right'}} variant="light" onClick={openModal}>+</Button>
                </Card.Header>
                <Card.Body>
                    <p>
                        {props.description}
                    </p>
                </Card.Body>
            </Card>
            { props.block_type === "nb_function" &&   
                <AddingBlockModal show={showModal} close={openModal} name={props.name} block_id={props.block_id} updateBlock={props.updateBlock}/>
            }
            { props.block_type === "output" && props.block_extra === "table" &&   
                <AddingBlockModal show={showModal} close={openModal} name={props.name} block_id={props.block_id} updateBlock={props.updateBlock}/>
            }
            { props.block_type === "output" && props.block_extra === "map" &&   
                <AddingBlockModal show={showModal} close={openModal} name={props.name} block_id={props.block_id} updateBlock={props.updateBlock}/>
            }
            { props.block_type === "output" && props.block_extra === "graphs" &&   
                <AddingBlockModal show={showModal} close={openModal} name={props.name} block_id={props.block_id} updateBlock={props.updateBlock}/>
            }
            { props.block_type === "input" && props.block_extra === "file" &&
                <UploadFileInput show={showModal} 
                                 close={openModal} 
                                 name={props.name} 
                                 block_id={props.block_id} 
                                 updateBlock={props.updateBlock}
                                 url={props.url} />
            }
            { props.block_type === "input" && props.block_extra === "text" &&
                <TextInputModal show={showModal} 
                                 close={openModal} 
                                 name={props.name} 
                                 block_id={props.block_id} 
                                 updateBlock={props.updateBlock} />
            }
            { props.block_type === "input" && props.block_extra === "multi_text" &&
                <MultiTextInputModal show={showModal} 
                                 close={openModal} 
                                 name={props.name} 
                                 block_id={props.block_id} 
                                 updateBlock={props.updateBlock} />
            }
        </>
        
    )
}