import React, { useState, useEffect } from 'react';
import { NotebookCard } from './NotebookCard';
import Accordion from 'react-bootstrap/Accordion';
import SquareLoader from "react-spinners/SquareLoader";

export const AccordionComponenet  = (props) => {
    let numberStyle = {
        borderRadius: '50%',
        width: '44px',
        height: '44px',
        padding: '10px',
        background: '#fff',
        border: '1px solid #000',
        color: '#000',
        textAlign: 'center',
      }

    let totalBlocks = Object.keys(props.blocks).filter(f => props.blocks[f]['name'].toLowerCase().includes(props.filter) || props.filter === '').length
    return (
            <Accordion.Item eventKey={props.accordionTitle}>
                <Accordion.Header style={{width:"100%"}}>
                    { props.loading.includes(props.accordionTitle) &&
                        <SquareLoader
                            color={"black"}
                            loading={true}
                            size={10}
                            aria-label="Loading Spinner"
                            data-testid="loader"
                        />
                    }
                    { !props.loading.includes(props.accordionTitle) &&
                        <span style={numberStyle}>{totalBlocks}</span>
                    }
                    &nbsp;{props.accordionTitle}
                    </Accordion.Header>
                <Accordion.Body>
                { totalBlocks!=0 &&
                    Object.keys(props.blocks).filter(f => props.blocks[f]['name'].toLowerCase().includes(props.filter) || props.filter === '').map((v, k) => {
                        return <NotebookCard key={v} name={props.blocks[v]['name']} 
                                            block_id={v} 
                                            block_type={props.blocks[v]['type']}
                                            block_extra={props.blocks[v]['extra']}
                                            description={props.blocks[v]['description']}
                                            updateBlock={props.updateBlock}
                                            loading={props.loading}
                                            url={props.url} />
                    })
                }
                { totalBlocks == 0 &&
                    <span>No blocks to show with filter...</span>
                }
                </Accordion.Body>
            </Accordion.Item>
    )
}