import React, { useState, useEffect } from 'react';
import { BlockArea } from './components/BlockArea';
import {BlockSideBar} from './components/BlockSideBar'
import {Header} from './components/Header'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import axios from 'axios';
import { genericBlocks } from './others/generic_blocks'

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

let backend_port = ".";

if(process.env.hasOwnProperty("REACT_APP_BACKEND_PORT")){
  backend_port = window.location.protocol + '//' + window.location.hostname + ":"+process.env.REACT_APP_BACKEND_PORT;
}
else{
  backend_port = ".";
}

const App = () => {
  // State Variables
  // New block added after deciding to create a new one.
  const [addedBlock, setAddedBlock] = useState([]);
  // All data that reflects the workflow to download.
  const [uploadedData, setUploadedData] = useState({"nodes":[],"edges":[]});
  const [uploadWorkflow, setUploadWorkdlow] = useState(false)

  // All data that reflects the workflow to download.
  const [inputOutputs, setInputOutputs] = useState("");

  // Notebooks been parsed
  const [parsingNotebooks, setParsingNotebooks] = useState({});

  const [allOutput, setAllOutputs] = useState([]);

  // Get Blocks from back-end
  const [blocks, setBlocks] = useState({})
  const getBlockData = () => {
      axios
          .get(backend_port+"/jbox_rest")
          .then(data => {
            let output = {...genericBlocks,...data.data}
              setBlocks(output)
          })
          .catch(error => console.log(error));
  };

  useEffect(() => {
    if(Object.keys(parsingNotebooks).length === 0 ){
      getBlockData();
    }
  },[parsingNotebooks]);

  useEffect(() => {
    console.log("In App.js")
  },[inputOutputs]);

  // Updating blocks when adding new ones!
  const updateBlockArea = (data) => {
    setAddedBlock(data);
  }

  return (
    <>
      <Header setData={setUploadedData} 
              data={uploadedData} 
              url={backend_port} 
              outputs={allOutput} 
              setOutputs={setAllOutputs}
              setUploadWorkdlow = {setUploadWorkdlow}
              parsingNotebooks={parsingNotebooks}
              setParsingNotebooks={setParsingNotebooks} />
      <Container>
        <Row style={{marginTop:"5px"}}>
          <Col md={3}>
            <BlockSideBar updateBlock={updateBlockArea} 
                          blocks={blocks} 
                          updateOutput={setInputOutputs} 
                          url={backend_port}
                          parsingNotebooks={parsingNotebooks}
                          setParsingNotebooks={setParsingNotebooks} />
          </Col>
          <Col md={9}>
            <BlockArea newData={addedBlock} 
                        blocks={blocks} 
                        data={uploadedData} 
                        newOutput={inputOutputs} 
                        uploadWorkflow = {uploadWorkflow}
                        setUploadWorkdlow = {setUploadWorkdlow}
                        setData={setUploadedData} 
                        backend_port={backend_port}
                        setOutputs={setAllOutputs}
                        allOutputs={allOutput} />
          </Col>
        </Row>
      </Container>
    </>
    
  );
};

export default App;