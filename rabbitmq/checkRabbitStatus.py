import pika

try:
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    print("RabbitMQ running on server!")

except:
    print("Please check that RabbitMQ is running on localhost")