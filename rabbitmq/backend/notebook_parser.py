import json
import uuid
import os
import shutil
import re
from os.path import dirname, abspath
from tokenize import generate_tokens
from io import StringIO 

venv_path = dirname(dirname(abspath(__file__))) + "/jbox_venv/"
backend_path = dirname(dirname(abspath(__file__))) + "/backend/"

import subprocess
import sys

import hashlib

# Code from: https://www.programiz.com/
def hash_file(filename):
    h = hashlib.sha1()

    with open(backend_path+"notebooks/"+filename,'rb') as file:
        chunk = 0
        while chunk != b'':
            chunk = file.read(1024)
            h.update(chunk)

    return h.hexdigest()

# Hashing function for ids
def hash_notebook_functions(text):
    h = hashlib.sha1(text.encode())
    return h.hexdigest()

# Installing Packages for Notebook
def install(packages):
    subprocess.check_call([sys.executable, "-m", "pip", "install" ]+packages)

# Getting notebook names from the direcotry of notebooks
def get_notebooks():
    notebooks = []
    directory = os.fsencode(backend_path+"notebooks/")
        
    for f in os.listdir(directory):
        filename = os.fsdecode(f)

        if filename.endswith(".ipynb"): 
           notebooks.append(filename)
    
    return notebooks

# Reading code from notebooks
def json_code(notebook):
    f = open(backend_path+"notebooks/"+notebook, 'r')
    Lines = f.readlines()
    return json.loads("".join(Lines))

# Store data to DB (JSON file for now)
def store_json(data):
    f = open(backend_path+"db.json", 'w')
    Lines = f.writelines(json.dumps(data))
    return 

# Opening file to store new functions
def open_py(f):
    f = open(backend_path+"py_code/"+f.split("/")[-1].split(".")[0]+".py", 'w')
    return f

# Generate function definition when storing.
def generate_def(id):
    return "def " + cells[id]['name'] + "("+cells[id]['inputs'].replace('[','').replace(']','')+"):"

# Getting pydoc from the function.
def get_py_doc(txt):
    regex = r'["""|\'\'\']([\w\s\(\)\.\-\,\;\:]+)["""|\'\'\']'
    x = re.search(regex, txt)
    
    return ' '.join(x.group(1).split()).replace("\n","").replace("\t", "")

# Function for adding the Celery annotations.
def create_jbox_tasks(data):
  result = []
  g = generate_tokens(StringIO(data).readline)
  for a, b, c, d, e in g:
          result.append(e)

  changes = {}
  for x in set(result):
    if "print(" in x:
        changes[x] = x.replace("print(", 'self.store_log(')
    elif "print (" in x[0:7]:
        changes[x] = x.replace("print (", 'self.store_log(')
    elif "def " in x[0:4]:
      splitted = x.split("(")
      changes[x] = splitted[0] + "(self, "+"".join(splitted[1:])

  for x in changes.keys():
    data = data.replace(x,changes[x])

  return "@app.task(bind=True,base=JBoxTask)\n"+data

# Generating JBoxTask
def generate_jbox_task():
    return """from celery_config.celery_app import app
import celery

class JBoxTask(celery.Task):
    def __init__(self):
        self.logs = {}
        celery.Task.__init__(self)

    def store_log(self, *args):
        log = args[0]
        if self.request.id not in self.logs:
            self.logs[self.request.id] = [log]
        else:
            self.logs[self.request.id].append(log)

        self.update_state(
            state="PENDING",
            meta={
                'output':None,
                'logs': self.logs[self.request.id]
            })

    def on_success(self, retval, uuid, args, kwargs):
        log = []
        if self.request.id in self.logs:
            log = self.logs[self.request.id]
        self.update_state(
            state="SUCCESS",
            meta={
                'output':retval,
                'logs': log
            })
    
    
    """

notebook_names = get_notebooks()

if len(notebook_names) == 0:
    print("No new files to process")

for book in notebook_names:
    print("Processing notebook: " + book)
    f = open(backend_path+"temp_files/"+book, "w")
    f.write("Now the file has more content!")
    f.close()
    file_id = hash_file(book)
    with open(backend_path+'db.json') as json_file:
        cells = json.load(json_file)
    code = json_code(book)
    py_files = open_py(backend_path+"notebooks/"+file_id+".ipynb")
    py_files.writelines(generate_jbox_task())
    # f = open(backend_path+"extra_code.py", 'r')
    # lines = f.readlines()
    # if len(lines) > 0:
    #     py_files.writelines(lines)
    for x in code['cells']:
        id = ""
        if x['cell_type'] == "code":
            def_func = False
            pydoc = False
            # Checking for JNB tag on comments.
            if len(x['source']) and  "# jbox" in x['source'][0]:
                splitted = x['source'][0].rstrip().replace("\n", "").replace("# jbox ", "").split(" ")
                if splitted[0] == "installations":
                    print("Installing notebook libraries")

                    # Clean values before going to subprocess.
                    packages = splitted[1:]
                    for ch in ['"',"'", ';','|']:
                        if ch in packages:
                            packages = packages.replace(ch,"")
                    install(packages)
                elif splitted[0] == "imports":
                    print("Adding imports")
                    for t_code in x['source'][1:]:
                        py_files.writelines(t_code)
                    py_files.writelines("\n")
                else:
                    temp = {"name": splitted[0]}
                    arg = ""
                    description = ""
                    for y in splitted[1:]:
                        if "-" == y[0]:
                            arg = y[1:]
                            if arg == "def":
                                def_func = True
                            if arg == "pydoc":
                                pydoc = True
                        else:
                            if arg == "description" and y[-1] != "'":
                                description = description +" "+ y
                            elif arg == "description" and y[-1] == "'":
                                temp[arg] = description + " " + y
                                temp[arg] = temp[arg].strip().replace("'","")
                            else:
                                temp[arg] = y
                    if pydoc:
                        temp['description'] = get_py_doc("".join(x['source']))

                    temp['file_name'] = file_id+".py"
                    temp['type'] = "nb_function"
                    temp['original_file'] = book
                    temp['extra'] = ""
                    id = hash_notebook_functions(str(temp))
                    cells[id] = temp
            
            cell_code = ""
            if not def_func and id != "":
                cell_code = cell_code+generate_def(id)+"\n"
            
            for t_code in x['source'][1:]:
                if def_func:
                    cell_code = cell_code+t_code

                else:
                    if id != "":
                        cell_code = cell_code+"    "+t_code

            if not def_func and id != "":
                cell_code = cell_code+"\n    return ("+(cells[id]['outputs'].replace("[","").replace("]",""))+")"

            cell_code = cell_code+"\n"
            if len(x['source']) and  "# jbox" in x['source'][0]:
                splitted = x['source'][0].replace("\n", "").replace("# jbox ", "").split(" ")
                if splitted[0] != "installations" and splitted[0] != "imports":
                    cell_code = create_jbox_tasks(cell_code)

            py_files.writelines(cell_code)
            
    store_json(cells)
    shutil.move(backend_path+"notebooks/"+book, backend_path+"notebooks/parsed/"+file_id+".ipynb")

    if os.path.exists(backend_path+"temp_files/"+book):
        print("Temp file removed")
        os.remove(backend_path+"temp_files/"+book)
    else:
        print("The file does not exist")