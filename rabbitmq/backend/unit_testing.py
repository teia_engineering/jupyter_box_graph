import unittest
import pathlib
import json
from importlib import import_module
from contextlib import redirect_stdout
import io
import pandas as pd
import glob
import time

def read_db(filename):
    with open(filename, 'r') as f:
        Lines = f.readlines()
        return json.loads("".join(Lines))
    
db = read_db(str(pathlib.Path(__file__).parent.resolve())+"/db.json")

testing_files = []
param_list = ""

for file in glob.glob("test/*.json"):
    testing_files.append(file)

def get_function(name, inputs):
    p, m = name.rsplit('.', 1)
    
    mod = import_module(p)
    met = getattr(mod, m)
    return met.delay(*inputs)

class JBoxEndPointTesting(unittest.TestCase):
    def test_jbox_converted_notebook(self):
        for p in self.param_list:
            db_data = db[p['id']]
            with self.subTest(msg=p['description'] ):
                
                task = get_function(".".join(["py_code",db_data["file_name"].split(".py")[0],db_data["name"]]), p['inputs'])
                task_info = task.backend.get_task_meta(task.id)
                
                while task_info['status'] == "PENDING":
                    time.sleep(2)
                    task_info = task.backend.get_task_meta(task.id)

                output = task_info['result']['output']
                output_list = []

                if "," not in db_data["outputs"] or ("," in db_data["outputs"] and type(output) != tuple):
                    output = (output)

                db_outputs = db_data['outputs'].strip('][')
                if "," in db_outputs:
                    db_outputs = db_outputs.split(",")
                else:
                    db_outputs = db_outputs.split()

                temp_out = {}
                if(len(db_outputs) == 1):
                    temp_out[db_outputs[0]] = output
                else:
                    for x in range(len(db_outputs)):
                        temp_out[db_outputs[x]] = output[x]

                self.assertEqual(p['assert_value'], temp_out)

class JBoxTextTestResult(unittest.TextTestResult):
    def addSubTest(self, test, subtest, err):
        super().addSubTest(test, subtest, err)
        self.testsRun+=1
        if err is not None:
            print("Failed", test.filename)
            
if __name__ == '__main__':
    runner = unittest.TextTestRunner(None, resultclass=JBoxTextTestResult, verbosity=2)

    suite = unittest.TestSuite()
    for  x in testing_files:
        with open(x) as f:
            param_list = json.load(f)
        test = JBoxEndPointTesting("test_jbox_converted_notebook")
        test.filename = x
        test.param_list = param_list
        suite.addTest(test)

    runner.run(suite)
