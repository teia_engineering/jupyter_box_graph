from main import app, tasks, db
import sys

def setMetadata(data):
    data['metadata']['Hello'] = "World"

    return data

before_lock = [
    setMetadata
]