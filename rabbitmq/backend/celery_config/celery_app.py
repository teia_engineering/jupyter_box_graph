from celery import Celery
from kombu.serialization import register
from .jbox_serializer import jbox_dumps, jbox_loads
from . import celeryconfig
import os

def detect_tasks(project_root):
    tasks = []
    file_path = os.path.join(project_root, 'py_code')
    for root, dirs, files in os.walk(file_path):
        for filename in files:
            if filename != '__init__.py' and filename.endswith('.py'):
                task = "py_code."+filename\
                    .replace(os.path.dirname(project_root) + '/', '')\
                    .replace('/', '.')\
                    .replace('.py', '')
                tasks.append(task)
    return tuple(tasks)

project_root = os.path.abspath(os.path.join(__file__ ,"../.."))
CELERY_IMPORTS = detect_tasks(project_root)

register('jboxSerializer', jbox_dumps, jbox_loads, 
    content_type='application/json',
    content_encoding='utf-8') 


app = Celery('jbox',
             task_serializer='jboxSerializer',
            result_serializer='jboxSerializer',
            accept_content=['jboxSerializer']
        )
app.config_from_object(celeryconfig)
app.autodiscover_tasks(lambda: CELERY_IMPORTS, force=True)

if __name__ == '__main__':
    app.start()