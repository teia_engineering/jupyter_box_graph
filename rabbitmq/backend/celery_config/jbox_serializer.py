import json
from datetime import datetime
from time import mktime

class JBoxEncoder(json.JSONEncoder):   
    def default(self, obj):
        if isinstance(obj, datetime):
            return {
                '__type__': '__datetime__', 
                'epoch': int(mktime(obj.timetuple()))
            }
        else:
            return json.JSONEncoder.default(self, obj)

def jbox_decoder(obj):
    if '__type__' in obj:
        if obj['__type__'] == '__datetime__':
            return datetime.fromtimestamp(obj['epoch'])
    return obj

def convert_dataframe(x):
    if 'dataframe' in str(type(x)).lower():
        return json.loads(x.to_json(orient="records"))
    else: 
        return x

def non_tuple(x):
    if 'dataframe' in str(type(x)).lower():
        return convert_dataframe(x)

    elif type(x) == list:
        output = []
        for y in x:
            output.append(convert_dataframe(y))
        return output

    elif type(x) == dict:
        output = {}
        for y in x:
            output[y] = convert_dataframe(x[y])
        return output
    else:
        return convert_dataframe(x)

# Encoder function      
def jbox_dumps(obj):
    if type(obj) == dict:
        if "status" in obj and  obj['status'] == "SUCCESS":
            output = []
            data = obj['result']
            log = False
            
            if type(obj['result']) == dict and "output" in obj['result'] and "logs" in obj['result']:
                data = obj['result']['output']
                log = True

            if type(data) == tuple:
                output = []
                for x in data:
                    
                    output.append(non_tuple(x))

                if log:
                    obj['result'] = {"output":output, "logs": obj['result']['logs']}
                else:    
                    obj['result'] = {"output":output}
                return json.dumps(obj, cls=JBoxEncoder)
            
            else:
                output = non_tuple(data)

                if log:
                    obj['result'] = {"output":output, "logs": obj['result']['logs']}
                else:    
                    obj['result'] = {"output":output}

                return json.dumps(obj, cls=JBoxEncoder)
        elif "status" in obj and  obj['status'] == "FAILURE":
            obj['traceback'] = str(obj['traceback'])
            return json.dumps(obj, cls=JBoxEncoder)
    return json.dumps(obj, cls=JBoxEncoder)

# Decoder function
def jbox_loads(obj):
    return json.loads(obj,object_hook=jbox_decoder)