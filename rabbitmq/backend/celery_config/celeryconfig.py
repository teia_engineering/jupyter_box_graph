from kombu.serialization import register

broker_url = 'amqp://jbox:jbox@localhost:5672/jbox_vhost'
result_backend = 'rpc'