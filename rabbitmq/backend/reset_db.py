import pathlib
import json
import shutil
import os

def read_db(filename):
    f = open(filename, 'r')
    Lines = f.readlines()
    return json.loads("".join(Lines))

db = read_db(str(pathlib.Path(__file__).parent.resolve())+"/db.json")

files = {}
for x in db.keys():
    files[db[x]['original_file']] = db[x]['file_name']

for x in files.keys():
    try:
        shutil.move(str(pathlib.Path(__file__).parent.resolve())+"/notebooks/parsed/"+files[x].split(".")[0]+".ipynb",str(pathlib.Path(__file__).parent.resolve())+"/notebooks/"+x)
        os.remove(str(pathlib.Path(__file__).parent.resolve())+"/py_code/"+files[x])
    except:
        print("File not found: ",x, files[x])

with open(str(pathlib.Path(__file__).parent.resolve())+"/db.json", 'w') as filetowrite:
    filetowrite.write('{}')
