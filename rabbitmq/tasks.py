from .celery import app
import time
import celery

class JBoxTask(celery.Task):
    def __init__(self):
        self.logs = {}
        celery.Task.__init__(self)

    def store_log(self, log):
        if self.request.id not in self.logs:
            self.logs[self.request.id] = [log]
        else:
            self.logs[self.request.id].append(log)

        self.update_state(
            state="PENDING",
            meta={
                'output':None,
                'logs': self.logs[self.request.id]
            })

    def on_success(self, retval, uuid, args, kwargs):
        self.update_state(
            state="SUCCESS",
            meta={
                'output':retval,
                'logs': self.logs[self.request.id]
            })
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        print('{0!r} failed: {1!r}'.format(task_id, exc))
        

@app.task(bind=True,base=JBoxTask)
def add(self,a, b):
    self.store_log("Hello")
    time.sleep(10)
    self.store_log("world!")
    time.sleep(10)
    return a+b


@app.task(bind=True, base=JBoxTask)
def mul(self,x, y):
    return x * y


@app.task(bind=True, base=JBoxTask)
def xsum(self,numbers):
    return sum(numbers)