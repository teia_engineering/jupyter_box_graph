.. convert_notebook:

Convert a notebook for JBox
===========================

To convert a notebook to be used in JBox you need to add comments into the cells 
you want to convert. There are 3 functions that are used, one for converting, the 
second one for installing needed packages for the notebook, and the third one for 
imports for the library. Every comment used will start the following way 
(remmeber the space between the # and jbox):

.. code:: python

    # jbox <commands>

Installing Packages
===================
In order to have the packages the notebooks need you can run the following line at the 
top of a cell. This is normally done at the first cell on the beginning of the notebook. 

.. code:: python

    # jbox installations <library_names>

You can have multiple libary names separated by spaces.

.. code:: python

    # jbox installations pandas numpy ipydatatable

Importing Code and libraries
============================
When looking to import a whole cell and not convert it into an end point you can use
the `import` command. Import can be use on any cell, for example a cell that contains
all the imports for the code or a cell with multiple functions.

.. code:: python

    # jbox imports
    import pandas as pd

    def testing():
        print("hello world)

Cell Conversion
===============
In order to convert a cell in a notebook into block in JBox there are multiple options. 
We will go over all the options availble and how to use them. Below you have all the 
options available.

    # jbox <block_name>
        [-inputs <array_of_input_names>]

        [-outputs <array_of_output_names> ]

        [-description <description_of_block>]

        [-pydoc] 

        [-def]

Cell with no inputs and outputs
-------------------------------
The most generic cell, with no inputs, outputs or a defined function.

.. code:: python

    # jbox hello_world -inputs [] -outputs [] -description 'Function to print Hello World!'
    print("Hello World!")


Cell with inputs and outputs
---------------------------- 
In this case you will have a cell with variables been used on the code that can be used as inputs and also variables that will be used as an output.

.. code:: python

    # jbox input_world -inputs [a,b] -outputs [c] -description 'Function with 2 inputs and one output.'

    c = str(a)+" "+str(b)


In this example the inputs are `a` and `b` and the output will be the `c`.

Defined function block
----------------------
In this case we are using a function that is already created and it already has a return. This one also has a pydoc used for the description of the block.

.. code:: python

    # jbox output_only_function -inputs [] -outputs [c,d] -pydoc -def

    def output_only_function():
        '''
            Function to show off the using a defined function and a 
            pydoc as the descriotion
        '''
        c = "Hello"
        d = "World"
        return [c,d]

Example Notebook
================
If you would like a notebook example to get started, JBox comes with one and can also
be found on the following location: `Gitlab <https://gitlab.com/teia_engineering/jupyter_box_graph/-/blob/main/backend/notebooks/testing.ipynb>`_.