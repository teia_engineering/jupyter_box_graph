.. backend_information:

Using the Standalone Backend
============================
The JBox backend can be used without the need to use the created frontend. 
You can replace the build directory with your own source code. This will allow
JBox to deploy the compile source code. You can also deploy JBox in a Docker
container and access the backend from another Docker container. On this page
we will only be covering the different endpoints that JBox provides and how 
to use them in order to create your requests to the backend.

Endpoints Available in JBox
===========================
JBox contains a set of default endpoints (more can be added) that allows applications
to interact with the JBox backend. 

/jbox_rest (GET)
----------------
This endpoint contains all the available functions that JBox has parsed
in the backend. It's a JSON object that contains a key and a value. The 
key is a hash for the function. This is done to have a unique key per parsed
function. The value assocaited to the key is a JSON object that has the
following values.

- **description**: Description provided to a function
- **extra**: Extra information that could be used inthe future.
- **file_name**: File name containing the parsed cells from the notebooks. (Hash of the file)
- **inputs**: A string containing the input variables available. These are in the format of [var1,var2,varn]
- **name**: Name of the function.
- **original_file**: Original file name.
- **outputs**: A string containing the output variables available. These are in the format of [var1,var2,varn]
- **type**: nb_functions is the default for any function parsed.

Below is an example of what a function looks like in the endpoint.

.. code:: python

    {
        49986303cfb12f77250f9242b7c37a02f3381838: {
            description: "Function to print Hello World!",
            extra: "",
            file_name: "c130f572ea4736b3b38236c802df972cfeb5cd90.py",
            inputs: "[]",
            name: "me45",
            original_file: "testing.ipynb",
            outputs: "[]",
            type: "nb_function"
        }
    }

/jbox_rest/upload_notebook (POST)
---------------------------------
Endpoint that allows you the user to upload a notebook
to the notebook directory in order to parse it in the 
backend. The endpoint receives a file from a multipart/form-data
content header. For example you can look at 
`Gitlab Repo <https://gitlab.com/teia_engineering/jupyter_box_graph/-/blob/main/src/components/modals/UploadFile.js>`_

Output Format
^^^^^^^^^^^^^
The output that gets returned from the backend looks like the following. You will see both a success
and a failure Below.

Successful output

.. code:: python

     {
        "children":[],
        "result":{
            "logs":["filename.ipynb saved in server."],
            "output":{}
        },
        "status":"SUCCESS",
        "task_id":"",
        "traceback":"filename.ipynb saved in server."
    }

Failure output

.. code:: python

     {
        "children":[],
        "result":{
            "logs":["File with non-allowable extension. Allowed externsions are ipynb"],
            "output":{}
        },
        "status":"FAILURE",
        "task_id":"",
        "traceback":"File with non-allowable extension. Allowed externsions are ipynb"
    }

/jbox_rest/input_upload_file (POST)
-----------------------------------
Endpoint that allows you the user to upload a file to the file directory. 
The endpoint receives a file from a multipart/form-data content header. 
For example you can look at 
`Gitlab Repo <https://gitlab.com/teia_engineering/jupyter_box_graph/-/blob/main/src/components/modals/UploadFile.js>`_

Output Format
^^^^^^^^^^^^^
The output that gets returned from the backend looks like the following. You will see both a success
and a failure Below.

Successful output

.. code:: python

     {
        "children":[],
        "result":{
            "logs":["/path/on/server/filename.csv saved in server."],
            "output":{
                "dir":"/path/on/server/filename.csv"
            }
        },
        "status":"SUCCESS",
        "task_id":"",
        "traceback":"/path/on/server/filename.csv saved in server."
    }

Failure output

.. code:: python

     {
        "children":[],
        "result":{
            "logs":["File with non-allowable extension. Allowed externsions are txt, jpg, png, jpeg, pdf, ipynb, xlsx, csv, doc, xls"],
            "output":{}
        },
        "status":"FAILURE",
        "task_id":"",
        "traceback":"File with non-allowable extension. Allowed externsions are txt, jpg, png, jpeg, pdf, ipynb, xlsx, csv, doc, xls"
    }

/jbox_rest/execute_box (POST)
-----------------------------
This endpoint is the one that executes the code blocks in the backend.
It uses the hash ID for the function as the way to determine what to call
and the inputs passed in as an array where each value in the array corresponds
to each value in the inputs value of the function. Below is an example of the
POST body. **Even if inputs is empty, it has to be populated.**

Example with no inputs

.. code:: python

    {
        "id":"49986303cfb12f77250f9242b7c37a02f3381838",
        "inputs":[]
    }

Example with inputs

.. code:: python

    {
        "id":"842220c372269eec93089c356afe50280d060a63",
        "inputs":["Hello","World!"]
    }

Output Format
^^^^^^^^^^^^^
The output that gets returned from the backend looks like the following. You will see both a success
and a failure Below.

Successful output

.. code:: python

     {
        "children":[],
        "result":{
            "logs":[],
            "output":{"job_id":"2753929b-fa63-4ff1-ac90-f037deb8f3c9"}
        },
        "status":"SUCCESS",
        "task_id":"",
        "traceback":null
    }

Failure output

.. code:: python

    {
        "children":[],
        "result":{
            "logs":["ID provided does not match any in the DB.json. Please try a new value."],
            "output":{}
        },
        "status":"FAILURE",
        "task_id":"",
        "traceback":"ID provided does not match any in the DB.json. Please try a new value."
    }

/jbox_rest/task_status (POST)
-----------------------------
This endpoint checks the status of the job running. It takes the **job_id**
from the **execute_box** endpoint output. If using the celery backend you
will get better feedback. This includes any print statement in the cells 
while its running can be seen in the logs. Also if there is an error
on the backend code running it will provide info on the traceback value.

Example of request

.. code:: python

    {
        "job_id":"132345f1-2599-4217-9ba1-ea5c8d68c4f6"
    }

Output Format
^^^^^^^^^^^^^
The output, if there is an output to be returned, can be found in the reult -> output.
In there you will have a dictionary with the output per output variables
that was available on the db.json.

.. code:: python

    {
        "children":[],
        "result":{
            "logs":[],
            "output":{
                "c":"hello world!"
            }
        },
        "status":"SUCCESS",
        "task_id":"d29f1eab-2602-41c3-bfbc-d3bb70a84d8f",
        "traceback":null
    }

