.. Jupyter Boxes documentation master file, created by
   sphinx-quickstart on Mon May 23 14:20:52 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Jupyter Boxes's documentation!
=========================================
Jupyter Boxes is a framework that allows a user to create a drag and drop workflow utilizing
Jupyter notebook cells. The framework is build on top of React Flow. A user will add comments 
to the beginning of a cell that will tell the framework how to convert it into a usable block. 
The framwork is composed a back-end/front-end (port 9494). The backend deployed 
contains the code for the frontend and the parsing code for the backend. File watcher looks
at the notebook directory to put new notebooks to be processed automatically. Once 
processed, the notebook is not looked at again (unless db reset). The processed information 
get stored into a file called db.json. This file contians all the blocks to display.

.. image:: example.png
   :width: 700px
   :height: 358px
   :alt: example image

.. toctree::
   :maxdepth: 4
   :caption: Getting Started

   getting_started

.. toctree::
   :maxdepth: 4
   :caption: Notebook Conversion

   convert_notebook

.. toctree::
   :maxdepth: 4
   :caption: Standalone Backend

   backend_information
      
.. toctree::
   :maxdepth: 4
   :caption: Configuring JBox Backend

   celery_conf