.. celery_conf:

Configuring JBox Backend
========================
JBox has several configuration options that can be applied to the backend. 
This includes, but not limited to, adding new endpoints, adding directory paths,
celery configuration, etc. Below we have explanations on how to work with this
features.

Adding new endpoints
====================
In order to add new endpoints, when doing Celery or not, there is a file called 
**extra_endpoints.py**. This file location can be found using the bash command 
provided by JBox. (**jbox_paths**) This command will tell you the location
for the extra endpoints file depending if doing celery or not. You can update
this file in order to create new endpoint in the backend. Below is an example
that can easily be added at the end of the file and that would add the endpoint
to the backend.

.. code:: python

    @app.route(path+'jbox_rest/extra_endpoints', methods=['GET'])
    def test():
        return 'Create more end points by adding them in this file.'

Just change the value after the **jbox_rest/**. Make sure it's unique and not in use
already by JBox. You also have access to the global variables from JBox as they are 
imported at the beginning of the file.

.. code:: python

    from main import app, tasks, db, path

Adding Configs to Flask
=======================
When adding a new endpoint you may want to have access to new directories
created or other configurations. In order for Flask to have knowledge of 
the directories or configs you can add them to the file called **extra_configs.py**. 
This file contains two JSON objects, one for configs and one for paths. If doing
paths add to **extra_file_paths**. This is important as JBox adds the required
paths to reach the directory.

.. code:: python

    extra_file_paths = {
        "certs":"certificate"
    }

    extra_configs = {
        "SECRET_KEY":"123456678"
    }

Configuring celery
==================
To configure celery there is a file called **celeryconfig.py** and can also be found by
the bash command provided by JBox. (**jbox_paths**) You can add all the configs required 
by celery. The configuration values avaialable to Celery can be found in the following
`link <https://docs.celeryq.dev/en/stable/userguide/configuration.html>`_.

