.. getting_started:

Installation
============

To install Jupyter boxes run the following command

.. code:: bash

    npm install jupyter_boxes -g

JBox commands
=============

Once installed, the library will contain a set of commands that can be run 
from the terminal. These commands will help on building the environment 
before you cam actually use the framework. 

jbox_build
----------

usage: jbox_build [--url_path <path>] [--no_frontend]

This command will build the python virtualenv for the backend to work. 
Will install all required libraries into a directory containing the virtual 
environment called `jbox_venv`. It also installs and build the front end.
Url path is used when building the frontend

jbox_reset_db
-------------

usage: jbox_reset_db [--rerun_notebooks true] [--celery true] 

This command will reset the backend "database" containing the notebooks 
that have been parsed. This will take any notebooks in the parsed directory 
and return them to be parsed again. It also restarts the db.json "database" file.

    `rerun_notebooks` - will move notebooks to rerun folder

    `celery` - will perform the reset on the celery/RabbitMQ directory.

jbox_start
----------

usage: jbox_start [--backend_port <9494>] [--cron_log true] [--celery]

This command will start the whole framework. This includes the backend, 
frontend, and cron schedule. There are two optional parameters, one for 
setting the backend-port and the second to allow logs to be created for 
the cron job. This file could be big so take into consideration. To run 
on the background add the `&` at the end.

    `backend_port` - Port to have the JBox running out of. Defaults to 9494

    `cron_log` - Notebook parsers can print alot of infomation, by setting cron_log
    it defaults to store into a log file.

    `celery` - will run the app on the celery/RabbitMQ directory.

.. code:: bash

    jbox_start --cron_log true --backend_port port_number --celery

jbox_start_back_end
-------------------

usage: jbox_start_back_end [--backend_port <9494>] [--url_path </>]

Starts the backend with a specified port. you can set a url path 
if you dont want it to be on the generic /. Meant for testing. 
If frontend is build, can be used the deployment.

.. code:: bash

    jbox_start_back_end --backend_port port_number --url_path /dns/

jbox_start_back_end_celery
--------------------------

usage: jbox_start_back_end_celery [--backend_port <9494>]

Starts the celery backend wihtout dameon. Meant for testing.

.. code:: bash

    jbox_start_back_end_celery --backend_port port_number

jbox_paths
----------

usage: jbox_paths 

Command to display paths available to configure JBox.

How to start
============

Once you have installed JBox as global, you will have access to all the above
commands on the terminal. Lets start by building the python and node environment.

.. code:: bash

    jbox_build

Once the command finishes, the system will have all the required python installs
and node. It will also build the frontend into the flask backend. Now there are
two backends JBox can use, a RabbitMQ/Celery and a Python/Thread version. Below 
we can see how to run both.

Celery/RabbitMQ
---------------
In order to use the Celery/RabbitMQ version, you need to have RabbitMQ installed
on your system (follow RabbitMQ website for installation. To run automatically 
the backend with RabbitMQ you would need to create a user and a virtualhost for 
JBox. Below are the commands by us. 

.. code:: bash

    sudo rabbitmqctl add_user jbox jbox 
    sudo rabbitmqctl add_vhost jbox_vhost
    sudo rabbitmqctl set_user_tags jbox administrator
    sudo rabbitmqctl set_permissions -p jbox_vhost jbox ".*" ".*" ".*"

Once the commands are ran you can start the app.

.. code:: bash

    jbox_start --celery true

Now you can go in localhost to port 9494 and you should have access to JBox.

    `http://localhost:9494/ <http://localhost:9494/>`_

Once you put a notebook on the notebooks directory and it gets parsed, you 
can access your db.json file from the browser by going go the 
`jbox_rest` endpoint.

    `http://localhost:9494/jbox_rest <http://localhost:9494/jbox_rest>`_

Thread backend
--------------

This one is more simple. Once the build command as finished you can run the 
`jbox_start` and you can go on from there.

.. code:: bash

    jbox_start

Now you can go in localhost to port 9494 and you should have access to JBox.

    `http://localhost:9494/ <http://localhost:9494/>`_

Once you put a notebook on the notebooks directory and it gets parsed, you 
can access your db.json file from the browser by going go the 
`jbox_rest` endpoint.

    `http://localhost:9494/jbox_rest <http://localhost:9494/jbox_rest>`_